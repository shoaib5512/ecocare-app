import BackgroundGeolocation from 'react-native-background-geolocation';
import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  AppRegistry,
  AsyncStorage,
  StatusBar,
  Platform,
} from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import * as Helper from './src/components/Helper';
import {getSession} from './src/network/Session';
import {getLocationPermission} from './src/network/Permission';
import AppNavigator from './src/navigation/AppNavigator';
var firebaseForToken = require('react-native-firebase');
import * as firebase from 'firebase';
// import 'firebase/firestore';
// import Geolocation from '@react-native-community/geolocation';
//import Geolocation from 'react-native-geolocation-service';
var androidAppId = '1:128908798235:android:460a71744425f202dc4bbe';
var iosAppId = '1:128908798235:android:460a71744425f202dc4bbe';
var firebaseConfig = {
  apiKey: 'AIzaSyADv-4X7dZRZ_5yazNZEsTthobktCWGVdM',
  authDomain: 'eco-care.firebaseapp.com',
  databaseURL: 'https://eco-care.firebaseio.com',
  projectId: 'eco-care',
  storageBucket: 'eco-care.appspot.com',
  messagingSenderId: '128908798235',
  appId: Platform.OS == 'ios' ? iosAppId : androidAppId,
};

export const app = firebase.initializeApp(firebaseConfig);
export const db = app.database();
export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      hasToken: false,
      isLoaded: false,
      location: [],
    };
    console.disableYellowBox = true;
  }

  async componentDidMount() {
  
  }

  async updateUserLocation() {
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    // var user = this.state.user;
    if (user != null) {
      if (Number(user.userType) == 3 && Number(user.isOnline) == 1) {
        let permission = await getLocationPermission();
        // console.warn(' > > > > > ', permission);
        if (permission) {
          Geolocation.getCurrentPosition(
            (position) => {
              //   this.setState({coords: position.coords});

              var coords = position.coords;
              console.warn('cords are ___ : ', coords);
              if (coords.latitude != undefined || coords != '') {
                //permission ok

                var userLocation = {
                  userId: user.userId,
                  latitude: coords.latitude,
                  longitude: coords.longitude,
                };

                this.setState({driverLocation: userLocation});
                // Helper.showSnack("coords.latitujjjjjde");
                // console.warn(
                //   ' + + + + + +  + + + + ++  + + + + + + +_______________ + + + + + + + ++ ',
                //   userLocation,
                // );
                Helper.updateUserLocation(userLocation);
              } else {
                
                this.setState({loader: false});
                Helper.showSnack('You cannot use current location feature.');
                // this.props.navigation.navigate(nav);
              }
            },
            (error) => {
              // console.warn('In Error conditions  * * * * * ** *  **: ', error);
              // Helper.showSnack(
              //   'Something went wrong while getting your location.',
              // );
              this.setState({loading: false, loader: false});
              //   this.props.navigation.navigate(nav);
            },

            {enableHighAccuracy: false, timeout: 10000, maximumAge: 10000},
          );
        } else {
          // console.warn('In else conditions  * * * * * ** *  **: ');
          this.setState({loading: false, loader: false});
          // Helper.showSnack('Something went wrong while getting your location.');
          //   this.props.navigation.navigate(nav);
        }
      }
    }
  }

  componentWillMount() {
    // if (Platform.OS != 'ios') {
    //   // this.updateUserLocation();
    //   BackgroundTimer.setInterval(() => {
    //     this.updateUserLocation();
    //   }, 5000);

    //   // this.interval = setInterval(() => this.updateUserLocation(), 15000);
    // }
    if (Platform.OS == 'ios') {
      ////
      // 1.  Wire up event-listeners
      //

      // This handler fires whenever bgGeo receives a location update.
      BackgroundGeolocation.onLocation(this.onLocation, this.onError);

      // This handler fires when movement states changes (stationary->moving; moving->stationary)
      BackgroundGeolocation.onMotionChange(this.onMotionChange);

      // This event fires when a change in motion activity is detected
      BackgroundGeolocation.onActivityChange(this.onActivityChange);

      // This event fires when the user toggles location-services authorization
      BackgroundGeolocation.onProviderChange(this.onProviderChange);

      ////
      // 2.  Execute #ready method (required)
      //
      BackgroundGeolocation.ready(
        {
          // Geolocation Config
          desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,
          distanceFilter: 10,
          // Activity Recognition
          stopTimeout: 1,
          notificationText: 'disable',
          // Application config
          debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
          logLevel: BackgroundGeolocation.LOG_LEVEL_OFF,
          stopOnTerminate: false, // <-- Allow the background-service to continue tracking when user closes the app.
          startOnBoot: true, // <-- Auto start tracking when device is powered-up.
        },
        (state) => {
          console.log(
            '- BackgroundGeolocation is configured and ready: ',
            state.enabled,
          );

          if (!state.enabled) {
            ////
            // 3. Start tracking!
            //
            BackgroundGeolocation.start(function () {
              console.log('- Start success');
            });
          }
        },
      );
    }
  }

  // You must remove listeners when your component unmounts
  componentWillUnmount() {
    if (Platform.OS == 'ios') {
      BackgroundGeolocation.removeListeners();
    }
  }
  onLocation(location) {
    console.log('[location] -', location.coords);
    Helper.updateGeoLocation(
      location.coords.latitude,
      location.coords.longitude,
    );
  }
  onError(error) {
    console.warn('[location] ERROR -', error);
  }
  onActivityChange(event) {
    // this.updateLocation(
    //   event.location.coords.latitude,
    //   event.location.coords.longitude,
    // );
    console.log('[activitychange] -', event); // eg: 'on_foot', 'still', 'in_vehicle'
  }
  onProviderChange(provider) {
    console.log('[providerchange] -', provider.enabled, provider.status);
  }
  onMotionChange(event) {
    // var loc = this.state.location;
    // var l = event.location.coords;
    // loc.push(l);
    // this.setState({location: loc});
    // alert(event.location.latitude);

    // var userLocation = {
    //   userId: 121212,
    //   latitude: event.location.coords.latitude,
    //   longitude: event.location.coords.longitude,
    // };

    // this.updateLocation(
    //   event.location.coords.latitude,
    //   event.location.coords.longitude,
    // );

    Helper.updateGeoLocation(
      event.location.coords.latitude,
      event.location.coords.longitude,
    );

    // console.log('[motionchange] -', event.isMoving, event.location);
    // console.log('[motionchange--------------] -', event.location);
  }

  async updateLocation(lat, long) {
    var user = await getSession();
    if (user != null) {
      if (user.userType == 3 && Number(user.isOnline) == 1) {
        var params = {
          userId: user.userId,
          latitude: lat,
          longitude: long,
        };
        Helper.updateUserLocation(params);
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.location.map((item, index) => {
          return <Text>{item.latitude}</Text>;
        })}
        <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
        <AppNavigator />
      </View>
    );
  }

  _handleLoadingError = (error) => {
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({isLoadingComplete: true});
  };
}

AppRegistry.registerComponent('Driver', () => Point);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
