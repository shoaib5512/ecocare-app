import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  styleSet,
} from '../../config/Constant';

export default StyleSheet.create({
  // all styles here

  mapView: {
    // ...StyleSheet.absoluteFillObject,
    // flex: 1,
    height: height - 80,
    marginLeft:-20,
    marginBottom:-20,
    width: width,
  },

  shadow: {
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 6,
  },

  borderRNone: {
    borderRightWidth: 0,
  },
  borderLNone: {
    borderLeftWidth: 0,
  },

  //Overlay
  height100: {
    height: screenHeight.height100,
  },
  bgOverlay: {
    backgroundColor: 'rgba(172,111,73,0.55)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
  },

  shadowContainer: {
    paddingVertical: 15,
    borderRadius: styleSet.bRadiusTheme,
    backgroundColor: '#fff',
    alignSelf: 'center',
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3,

    elevation: 3,
  },

  actionSheet: {
    backgroundColor: colors.white,
    minHeight: screenHeight.height20,
  },
  actionSheetLg: {
    backgroundColor: colors.pureWhite,
    minHeight: screenHeight.height30,
  },
  actionSheetContainer: {
    padding: 15,
    flex: 1,
    width: '100%',
  },

  //otp styling
  otp: {
    width: screenWidth.width15,
    // width:45,
    // width:'25%',
    height: 45,
    borderRadius: 0,
    // backgroundColor: colors.pureWhite,
    color: colors.black,
    fontSize: 16,
    borderWidth: 0,
    borderBottomWidth: 1.5,
    borderBottomColor: colors.placeholder,
    fontFamily: 'Poppins-Regular',
  },

  otpHighlighted: {
    borderBottomColor: colors.themeDefault,
  },

  taskContainer: {
    backgroundColor: colors.white,
    borderRadius: 6,
    padding: 12,
    marginBottom: 20,
    minHeight: 150,
    borderWidth: 0.5,
    borderColor:'red',
  },
  routeContainer: {
    backgroundColor: colors.white,
    borderRadius: 6,
    padding: 12,
    marginBottom: 20,
    borderWidth:0.5,
    borderColor:'red',
  },
  settingsHeader: {
    backgroundColor: colors.themeDefault,
    paddingTop: 38,
    paddingBottom: 60,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingLeft: 30,
  },
  borderBottom: {borderBottomWidth: 1, borderBottomColor: '#ECECEC'},
  routeMapIcon: {position: 'absolute', bottom: 8, right: 8},

  tabsLi: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 15,
    borderBottomWidth: 1,
  },
  addImgIcon: {
    position: 'absolute',
    bottom: -12,
    left: screenWidth.width20 / 2.2,
    zIndex: 10,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 6,
  },
  btnLoadMoreContainer:{
    // backgroundColor:'#fff',
    position: 'absolute',
    zIndex:4,
    elevation:10,
    height:110,
    width:'100%',
    alignSelf:'center',
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center',
    bottom:0
  }
});
