import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';

export default StyleSheet.create({
  buttonTheme: {
    backgroundColor: colors.themeDefault,
    width: '100%',
    // alignSelf: 'center',
    paddingVertical: 15,
    paddingHorizontal:8,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'center',
    marginTop: 25,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 3,
  },
  buttonThemeOutline: {
    backgroundColor: colors.white,
    // width: '100%',
    flex: 1,
    // alignSelf: 'center',
    // paddingVertical:13,
    borderRadius: 10,
    justifyContent: 'center',
    // alignItems: 'center',
    marginTop: 25,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,
    elevation: 6,
    borderWidth: 1,
    borderColor: colors.themeSecondary,
  },

  btnRoute:{
    backgroundColor: colors.black,
    padding: 10,
    borderRadius: 8,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,
    elevation: 6,
  }
  
});
