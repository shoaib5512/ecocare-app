import { StyleSheet, Dimensions } from 'react-native';
const { height, width } = Dimensions.get('screen');
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  styleSet,
} from '../../config/Constant';

export default StyleSheet.create({
  // alll images  style here
  iconXsm: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
  },

  iconSm: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },

  icon: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
  },
  iconMd: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  iconSettingsArrow: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
    tintColor: colors.themeDefault
  },
  
 
 
  iconLg: {
    resizeMode: 'contain',
    maxWidth: 140,
    height: 80,
    width: '100%',
    position: 'relative',
    zIndex: 2,
    marginBottom: 80,
    alignSelf: 'center',
  },
  
 

  bgImageContainer: {
    width: screenWidth.width100,
    flex: 1,
    resizeMode: 'stretch',
    backgroundColor: colors.white
  },
  logoImg: {
    resizeMode: 'contain',
    maxWidth: 200,
    height: 200,
    width: '100%',
    position: 'relative',
    zIndex: 2,
    // marginTop: 20,
    alignSelf: 'center',
  },
 
  checkBox: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  locationToIcon:{ width: 14, height: 14, resizeMode: 'contain' },
  locationFromIcon:{ width: 14, height: 16, resizeMode: 'contain' },

 

  //tints
  tintWhite: {
    tintColor: colors.white,
  },
  tintDefault: {
    tintColor: colors.themeDefault,
  },

  profile:{
    width: screenWidth.width20,
    height:  screenWidth.width20,
    resizeMode:'cover',
    borderRadius: 300,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 6,
    borderWidth:2,
    borderColor:'#eee',
  },
  profileSm:{
    width: 30,
    height: 30,
    resizeMode:'cover',
    borderRadius: 300,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 6,
  }

});
