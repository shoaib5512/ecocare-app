import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  fontSet,
  styleSet
} from '../../config/Constant';
import text from './text';

export default StyleSheet.create({
  // all input here
  labelInput: {
    color: colors.themeDefault,
    fontSize: fontSet.normal,
    fontFamily:'Roboto-Medium'
  },
  formInput: {
    // borderBottomWidth: 1.5,
    // borderColor: colors.themeDefault,
    backgroundColor:'#F9F9F9',
    marginVertical: 12,
    paddingVertical: 12,
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 3,
    borderRadius:6

  },
  input: {
    borderWidth: 0,
    color: colors.black,
    padding: 0,
    paddingTop: 5,
    fontSize: fontSet.normal,
    fontFamily: 'Poppins-Regular',
    paddingHorizontal: 10,
    

  },
  



  textarea: {
    borderRadius: 4,
    borderWidth: 0.8,
    borderColor: colors.grey,
    backgroundColor: colors.white,
    fontSize: fontSet.normal,
    height: 100,
    fontFamily:'RobotoCondensed-Light',
    marginTop: 10,
    marginBottom: 10,
    textAlignVertical: 'top',
    paddingHorizontal: 10,
  },
  searchInput: { height: 45, paddingLeft: 15, paddingRight: 10 }
});
