import { StyleSheet, Dimensions } from 'react-native';
const { height, width } = Dimensions.get('window');
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  fontSet,
} from '../../config/Constant';

export default StyleSheet.create({
  fontFamily: {
    // fontFamily:'serif',
  },
  text6: {
    fontSize: 6,
  },
  text7: {
    fontSize: 7,
  },
  text8: {
    fontSize: 8,
  },
  text9: {
    fontSize: 9,
  },
  text10: {
    fontSize: 10,
  },
  text11: {
    fontSize: 11,
  },
  text12: {
    fontSize: 12,
  },
  text14: {
    fontSize: 14,
  },
  text15: {
    fontSize: 15,
  },
  text16: {
    fontSize: 16,
  },
  text18: {
    fontSize: 18,
  },
  text20: {
    fontSize: 20,
  },
  text22: {
    fontSize: 22,
  },
  text23: {
    fontSize: 23,
  },
  text24: {
    fontSize: 24,
  },
  text25: {
    fontSize: 25,
  },
  text26: {
    fontSize: 26,
  },
  text27: {
    fontSize: 27,
  },
  text30: {
    fontSize: 30,
  },
  letterSpace: {
    letterSpacing: 1,
  },

  justify: {
    textAlign: 'justify',
  },
  center: {
    textAlign: 'center',
  },
  right: {
    textAlign: 'right',
  },
  left: {
    textAlign: 'left',
  },
  lineHeight:{
    lineHeight:20
  },

  regular: {
    fontFamily: 'Poppins-Regular',
  },
  medium: {
    fontFamily: 'Poppins-Medium',
  },
  semibold: {
    fontFamily: 'Poppins-SemiBold',
  },
  bold: {
    fontFamily: 'Poppins-Bold',
  },
  light: {
    fontFamily : 'Poppins-Thin'
  },

 
  


  //theme Fonts
  headertextstyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hl: {
    fontSize: fontSet.large,
    fontFamily: 'Poppins-Bold',
    color: colors.themeDefault,
    textAlign: 'left',
    letterSpacing: 1,
  },
  h1: {
    fontSize: fontSet.middle,
    fontFamily: 'Poppins-Bold',
    color: colors.themeDefault,
    textAlign: 'left',
    letterSpacing: 1,
  },
  h2: {
    fontSize: fontSet.middle,
    fontFamily: 'Poppins-Medium',
    color: colors.themeDefault,
    textAlign: 'left',
    letterSpacing: 1,
  },
  h3: {
    fontSize: fontSet.normal,
    fontFamily: 'Poppins-Regular',
    color: colors.black,
  },
  h4: {
    fontSize: fontSet.small,
    fontFamily: 'Poppins-Medium',
    color: colors.greyDark,
  },
  h5: {
    fontSize: fontSet.xsmall,
    fontFamily: 'Poppins-Regular',
    color: colors.greyDark,
  },
  
  title: {
    fontSize: fontSet.middle,
    fontFamily: 'Poppins-Medium',
    color: colors.black,
    letterSpacing: 1,
  },
  para: {
    fontSize: fontSet.normal,
    fontFamily: 'Poppins-Regular',
    color: colors.grey,
    letterSpacing: 1,
  },
  screenTitle: {
    fontSize: fontSet.middle,
    fontFamily: 'Roboto-Bold',
    color: colors.themeDefault,
    textAlign: 'center',
  },
  
  //fontFamilies
  robotoBold: {
    fontFamily: 'Roboto-Bold',
  },
  robotoCondensedBold: {
    fontFamily: 'RobotoCondensed-Bold',
  },
  robotoCondensedRegular: {
    fontFamily: 'RobotoCondensed-Regular',
  },
  robotoCondensedLight: {
    fontFamily: 'RobotoCondensed-Light',
  },

  robotoMedium: {
    fontFamily: 'Roboto-Medium',
  },
  robotoRegular: {
    fontFamily: 'Roboto-Regular',
  },
  robotoLight: {
    fontFamily: 'Roboto-Light',
  },
  //font colors
  white: {
    color: colors.white,
  },
  greyDark: {
    color: colors.greyDark,
  },
  black: {
    color: colors.black,
  },
  light: {
    color: colors.greyDark,
  },
  skyBlue: {
    color: colors.skyBlue,
  },
  themeDefault: {
    color: colors.themeDefault,
  },
  themeSecondary:{
    color:colors.themeSecondary
  },
  
});
