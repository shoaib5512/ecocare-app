import React from 'react';
import {AsyncStorage} from 'react-native';
//import CookieManager from 'react-native-cookies';

async function verifySession() {
  var userSession = await AsyncStorage.getItem('user');
  var locationPermission = await AsyncStorage.getItem('locationPermission');
  locationPermission = JSON.parse(locationPermission);
  var nav = 'Login';
  if (locationPermission != null) {
    console.log(locationPermission);
    userSession = JSON.parse(userSession);

    if (userSession != null) {
      if (userSession.userType == 2) {
        nav = 'Home';
      } else if (userSession.userType == 3) {
        nav = 'DriverTask';
      } else {
        nav = 'TaskScanner';
        nav = 'QRDetail';
         nav = 'Admin';
      }
    }
  } else {
    nav = 'LocationPermission';
  }
  return nav;
}

async function getSession() {
  let userSession = await AsyncStorage.getItem('user');
  let user = JSON.parse(userSession);
  return user;
}
async function logout() {
  let userSession = await AsyncStorage.getItem('user');
  let nav = 'Login';
  console.log(nav);
  if (userSession != null) {
    nav = 'Login';
  }
  await AsyncStorage.removeItem('user');
  return true;
}

{
  /*async function logout() {
  CookieManager.clearAll().then(res => {
    console.log('CookieManager.clearAll =>', res);
  });
  await AsyncStorage.removeItem('user');
  return true;
}*/
}

export {verifySession};
export {getSession};
export {logout};
