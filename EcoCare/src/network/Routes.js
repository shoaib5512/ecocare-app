import React, {Component} from 'react';

const url = {
  // base: 'https://test-rexsolution.com/ecocare/api/',
  base: 'http://88.85.125.77/ecocare/api/',
  login: 'UserService/login/',
  forgotPassword: 'UserService/forgotPassword/',
  getDriverTask: 'FileService/getDriverTask/',
  getTaskRoutes: 'FileService/getTaskRoutes/',
  updateRouteStatus: 'FileService/updateRouteStatus/',
  getUserObject: 'FileService/getUserObject/',
  updateProfile: 'FileService/updateProfile/',
  updateUserStatus: 'FileService/updateUserStatus/',
  getAllTask : 'FileService/getAllTask/',
  Logout: 'Logout/',
  updateRouteStat: 'FileService/updateRouteStat/', // for testing
  getBarCodeData: 'FileService/getBarCodeData/', // for testing
};
export {url};
