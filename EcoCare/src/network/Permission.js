import {Platform, PermissionsAndroid} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
export async function getLocationPermission() {
  var response = false;

  if (Platform.OS === 'ios') {
    Geolocation.setRNConfiguration({
      skipPermissionRequests: false,
      authorizationLevel: 'always',
    });
    Geolocation.requestAuthorization();
    response = true;
  } else {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'E-Delivery',
          message: 'E-Delivery wants to access your device location to track your current location upadtes. ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // alert("You can use the location");
        //
        console.warn(' _ _  _1  _ _ _ ', granted);
        // return true;
        const grantedBackground = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
          {
            title: 'E-Delivery',
            message: 'E-Delivery wants to access your device location to track your live location updates, even when the app is closed or not in use,',
          },
        );
        console.warn(' _ _ _2 _ _ _', grantedBackground);
        response = true;
      } else {
        alert(
          'Location permission denied, you cannot use location features in the application. Please enable from settings location to view location based services.',
        );
      }
    } catch (err) {
      // alert(err);
      console.warn(' _in catch  _ _ _ ', err);
      // response = true;
    }
  }

  return response;
}


export async function getWritePermission() {
  var response = false;


    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'E-Delivery',
          message: 'E-Delivery wants to save signature images.. ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // alert("You can use the location");
        //
        console.warn(' _ _  _1  _ _ _ ', granted);
        return true;
      }
    } catch (err) {
      // alert(err);
      console.warn(' _in catch  _ _ _ ', err);
      // response = true;
    }
  

  return response;
}


export async function getReadPermission() {
  var response = false;
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'E-Delivery',
          message: 'E-Delivery wants to read your gallery. ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // alert("You can use the location");
        //
        console.warn(' _ _  _1  _ _ _ ', granted);
        return true;
      }
    } catch (err) {
      // alert(err);
      console.warn(' _in catch  _ _ _ ', err);
      // response = true;
    }
  

  return response;
}