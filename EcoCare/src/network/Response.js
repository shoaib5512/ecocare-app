import React, {Component} from 'react';
import {url} from './Routes';
import {AsyncStorage} from 'react-native';

export async function login(params) {
  var link = url.base + url.login;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}


export async function signup(params) {
  var link = url.base + url.signup;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}

export async function forgotPassword(params) {
  var link = url.base + url.forgotPassword;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}


export async function updateUserStatus(params) {
  var link = url.base + url.updateUserStatus;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}


export async function getDriverTask(params) {
  var link = url.base + url.getDriverTask;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}



export async function getTaskRoutes(params) {
  var link = url.base + url.getTaskRoutes;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}


export async function getAllTask(params) {
  var link = url.base + url.getAllTask;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}
export async function updateRouteStatus(params) {
  var link = url.base + url.updateRouteStat;
  let formData = new FormData();
  console.log('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ', params);
  if (Number(params.status)  == 2) {
    let imageUrl = params.image;
    let uri = Platform.OS === 'ios' ? imageUrl.replace('file://', '') : imageUrl;
    console.log(params['image']);

    let localUri = decodeURIComponent(uri);
    console.warn(localUri);
    let filename = localUri.split('/').pop();
    let match = /\.(\w+)$/.exec(filename); 

    // let type = match ? `image/${match[1]}` : `image`;
    let type = 'image/jpeg';
    formData.append(
      'image',
      typeof {uri: localUri, name: filename, type} != 'undefined'
        ? {uri: localUri, name: filename, type}
        : 0,
    );
    formData.append('receiverName', params.receiverName);
  }

  formData.append('userId', params.userId);
  formData.append('sessionId', params.sessionId);
  formData.append('status', params.status);
  formData.append('routeId', params.routeId);
  console.log(formData['_parts']); 
  try {
    let response = await fetch(link, {
      method: 'POST',
      body: formData,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    });

    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log('error:', error);
    return error;
  }
}

export async function Logout(params) {
  var link = url.base + url.Logout;
  try {
    console.warn('------------------:', params);
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });

    let responseJson = await response.json();
    console.log('RESPONSE :', responseJson);
    return responseJson;
  } catch (error) {
    console.log('error:', error);
  }
}




//update Profile
//update service provider profile
export async function updateProfile(params) {
  var link = url.base + url.updateProfile;
  let formData = new FormData();

  //image
  if (params.image != '') {
    var imageUrl = params.image;
    var uri = Platform.OS === "ios" ? imageUrl.replace("file://", "") : imageUrl;
    let localUri = decodeURIComponent(uri);
    let filename = localUri.split('/').pop();

    let match = /\.(\w+)$/.exec(filename);

    // let type = match ? `image/${match[1]}` : `image`;
    let type = 'image/jpeg';
    formData.append(
      'image',
      typeof {uri: localUri, name: filename, type} != 'undefined'
        ? {uri: localUri, name: filename, type}
        : 0,
    );
  }

  formData.append('userId', params.userId);
  formData.append('phone', params.phone);
  formData.append('email', params.email);
  formData.append('firstName', params.firstName);
  formData.append('lastName', params.lastName);
  formData.append('sessionId', params.sessionId);
  try {
    let response = await fetch(link, {
      method: 'POST',
      body: formData,
      headers: {
        Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      },
    });
    console.warn(response);
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log('error:', error);
    return error;
  }
}


//chnge Passsword
export async function changePassword(params) {
  var link = url.base + url.updateProfile;
  // console.warn(params);
  try {
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });
    // console.warn(response);
    var responseJson = await response.json();
    return responseJson;
  } catch (response) {
    console.log('error:', response.message);
  }
}

//get user obj
export async function getUserObject(params) {
  var link = url.base + url.getUserObject;
  try {
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });
    // console.warn(response);
    var responseJson = await response.json();
    return responseJson;
  } catch (response) {
    console.log('error:', response.message);
  }
}
//get user obj
export async function getBarCodeData(params) {
  var link = url.base + url.getBarCodeData;
  try {
    let response = await fetch(link, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });
    // console.warn(response);
    var responseJson = await response.json();
    return responseJson;
  } catch (response) {
    console.log('error:', response.message);
  }
}


