import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  PermissionsAndroid,
  AsyncStorage,
  Linking,
} from 'react-native';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {CameraKitCameraScreen} from 'react-native-camera-kit';
import {RNCamera} from 'react-native-camera';
import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
  ButtonDefaultwhite,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {getBarCodeData} from '../../network/Response';
import * as Helper from '../../components/Helper';

export default class TaskScanner extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      tasks: [],
      loader: false,
      loading: false,
      qrvalue: '',
      opneScanner: false,
      routes: [],
      qrData: {
        userName: '',
        labName: '',
        creationDate: '',
        receptionDate: '',
        coolboxId: '',
      },
    };
    this.onBarcodeScan = this.onBarcodeScan.bind(this);
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Scanner',
    headerStyle: {
      backgroundColor: colors.white,
    },
    // headerTitleAlign: 'center',

    headerTitleStyle: {
      color: colors.themeDefault,
      // textAlign: 'center',
    },
  };

  componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.onOpenScanner();
    });
    this.onOpenScanner();
  }

  goBackToRoute() {
    const {navigation} = this.props;
    navigation.goBack();
    navigation.state.params.onSelect();
  }

  async onBarcodeScan(qrvalue) {
    //  var  that = this;
    console.log(qrvalue.data);

    // alert(qrvalue.data);

    this.setState({loading: true});
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var params = {};
    params['userId'] = user.userId;
    params['sessionId'] = user.sessionId;
    params['code'] = qrvalue.data;
    // this.props.navigation.state.params.code;
    this.setState({user: user, code: params['code']});
    let response = await getBarCodeData(params);

    let totalCollectionArray = response.data.listLabReceptionSample;
    let collectionCount = totalCollectionArray
      ? totalCollectionArray.length
      : 0;

    // this.setState({loading: false});
    // console.log(response.data.listLabReceptionSample[0]['dateTimeOfReception']);
    console.log('response.data.status', response.data.status);
    if (response.status == 200) {
      this.setState({loading: false});
      if (response.data.status && response.data.status == 404) {
        Helper.showSnack('Invalid Qr Code');
     
        return;
      }
      var qrData = this.state.qrData;
      qrData['coolboxId'] = response.data.coolboxId;
      qrData['barcode'] = response.data.barcode;
      qrData['locationName'] = response.data.locationName;

      qrData['labName'] = response.data.labName;
      qrData['userName'] = response.data.userName;
      qrData['receptionDate'] =
        response.data.listLabReceptionSample[0]['dateTimeOfReception'];
      qrData['creationDate'] = response.data.dateTimeOfCreation;
      qrData['collectionCount'] = collectionCount;
      this.setState({opneScanner:false, loading: false, qrData: qrData, });
    } else if (response.status == 400) {
      this.setState({loading: false});
      Helper.showSnack(response.message);
    } else {
      this.setState({loading: false});
      Helper.showSnack(response.message);
    }
  }
  async onOpenScanner() {
    var that = this;
    //To Start Scanning
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'Eco Care App Camera Permission',
              message: 'Eco Care App needs access to your camera ',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            that.setState({qrvalue: ''});
            that.setState({opneScanner: true});
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    } else {
      // const isUserAuthorizedCamera = await Camera.requestDeviceCameraAuthorization();

      // if (isUserAuthorizedCamera) {
      that.setState({qrvalue: ''});
      that.setState({opneScanner: true});
      //}
    }
  }

  render() {
    if (!this.state.opneScanner) {
      return (
        <SafeAreaView style={[appStyle.flex1]}>
          <View style={[appStyle.mainContainer]}>
            <KeyboardAwareScrollView
              contentContainerStyle={[appStyle.scrollContainerCenter]}>
              <View style={[appStyle.wrapper]}>
                <View style={[appStyle.mb10]}>
                  <Text style={[text.title, {fontSize: 16}]}>
                    Cool Box Id: {this.state.qrData.coolboxId}
                  </Text>
                  <Text style={[text.title, {fontSize: 16}]}>
                    Barcode: {this.state.qrData.barcode}
                  </Text>
                  <Text style={[text.title, {fontSize: 16}]}>
                    Lab Name: {this.state.qrData.labName}
                  </Text>
                  <Text style={[text.title, {fontSize: 16}]}>
                    Location Name: {this.state.qrData.locationName}
                  </Text>
                  <Text style={[text.title, {fontSize: 16}]}>
                    User Name: {this.state.qrData.userName}
                  </Text>
                  <Text style={[text.title, {fontSize: 16}]}>
                    Creation Name: {this.state.qrData.creationDate}
                  </Text>
                  <Text style={[text.title, {fontSize: 16}]}>
                    Collection Count: {this.state.qrData.collectionCount}
                  </Text>

                  {/* <Text style={[text.h4, text.medium]}>
             sdf
                </Text> */}
                </View>

                <View>
                  {this.state.routes.length > 0 &&
                    this.state.routes.map((item, index) => {
                      return (
                        <View style={[appStyle.shadow, style.routeContainer]}>
                          <View
                            style={[
                              appStyle.rowBtw,
                              appStyle.aiFlexStart,
                              appStyle.pv10,
                            ]}>
                            <View style={[appStyle.flex1, appStyle.pr20]}>
                              {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                              <View
                                style={[appStyle.row, appStyle.aiFlexStart]}>
                                <Image
                                  source={images.locationFrom}
                                  style={image.locationFromIcon}
                                />
                                <Text
                                  style={[
                                    text.h4,
                                    text.black,
                                    text.medium,
                                    appStyle.ml10,
                                  ]}>
                                  {item.startAddress}
                                </Text>
                              </View>
                              <View
                                style={[
                                  appStyle.row,
                                  appStyle.mt5,
                                  appStyle.aiCenter,
                                ]}>
                                <Image
                                  source={images.locationTo}
                                  style={image.locationToIcon}
                                />

                                <Text
                                  style={[text.h4, text.medium, appStyle.ml10]}>
                                  {item.endAddress}
                                </Text>
                              </View>
                              <Text
                                style={[
                                  text.h5,
                                  appStyle.ml5,
                                  {paddingLeft: 20},
                                ]}>
                                Distance: {item.distanceText}
                              </Text>
                              <Text
                                style={[
                                  text.h5,
                                  appStyle.ml5,
                                  {paddingLeft: 20},
                                ]}>
                                Duration: {item.durationText}
                              </Text>
                              <Text
                                style={[
                                  text.h5,
                                  appStyle.ml5,
                                  {paddingLeft: 20},
                                ]}>
                                Collections: {item.collectionCount}
                              </Text>
                            </View>
                            {this.getStatusIcon(item.status)}
                          </View>
                          <View>
                            <TouchableOpacity
                              onPress={() => this.onSelect(item, index)}
                              style={style.routeMapIcon}
                              hitSlop={hitArea.hitArea}>
                              <Image
                                source={images.route}
                                style={[image.iconMd, appStyle.asFlexEnd]}
                              />
                            </TouchableOpacity>
                          </View>
                          {/* )} */}
                        </View>
                      );
                    })}
                  <View>
                    <View style={appStyle.mt30}>
                      <Text style={[text.title, {fontSize: 16}]}>
                        <Text style={{color: 'red'}}>*</Text>Kindly press
                        confirm that you picked this box
                      </Text>
                    </View>
                    <View style={[appStyle.rowBtw, {padding: 5}]}>
                      <ButtonDefault
                        onPress={() => this.goBackToRoute()}
                        maxWidth={'40%'}>
                        Confirm
                      </ButtonDefault>
                      <ButtonDefaultwhite
                        onPress={() => this.props.navigation.pop(2)}
                        maxWidth={'40%'}>
                        Cancel
                      </ButtonDefaultwhite>
                    </View>
                  </View>
                </View>
              </View>
            </KeyboardAwareScrollView>
          </View>
        </SafeAreaView>
      );
    }

    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper, {paddingHorizontal: 0}]}>
              <View>
                <View style={{flex: 1}}>
                  <QRCodeScanner
                    onRead={this.onBarcodeScan}
                    showMarker={true}
                    // flashMode={RNCamera.Constants.FlashMode.torch}
                    cameraProps={{captureAudio: false}}
                    reactivate={true}
                    topContent={
                      <Text style={styles.centerText}>Scan QR code</Text>
                    }
                  />
                </View>
              </View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loading}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width: 300,
    // marginTop: 16,
  },
  heading: {
    color: 'black',
    fontSize: 24,
    alignSelf: 'center',
    padding: 10,
    marginTop: 30,
  },
  simpleText: {
    color: 'black',
    fontSize: 20,
    alignSelf: 'center',
    padding: 10,
    marginTop: 16,
  },
  /////
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 10,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
