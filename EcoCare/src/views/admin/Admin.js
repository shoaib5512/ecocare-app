import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  PermissionsAndroid,
  AsyncStorage,
  Linking,
} from 'react-native';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {CameraKitCameraScreen} from 'react-native-camera-kit';
import {RNCamera} from 'react-native-camera';
import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
  ButtonDefaultwhite,
  ButtonDefaultSlim,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {getBarCodeData} from '../../network/Response';
import * as Helper from '../../components/Helper';

export default class TaskScanner extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      tasks: [],
      loader: false,
      qrvalue: '',
      opneScanner: false,
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Admin',
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTitleAlign: 'center',

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
  };

  async componentDidMount() {}

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>Welcome!</Text>
              </View>
              <View>
                <View style={[appStyle.rowBtw, {padding: 5}]}>
                  <ButtonDefault
                    onPress={() => this.props.navigation.navigate('Logout')}
                    maxWidth={'45%'}>
                    Logout
                  </ButtonDefault>

                  <ButtonDefaultwhite
                    onPress={() =>
                      this.props.navigation.navigate('TaskScanner')
                    }
                    maxWidth={'45%'}>
                    Open Scanner
                  </ButtonDefaultwhite>
                </View>
              </View>
              <View></View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
