import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  hitArea,
} from '../../config/Constant';
var firebase = require('firebase');
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import LeftComponent from '../../components/headerComponent/LeftComponent';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
  ButtonDefaultwhite,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {getBarCodeData} from '../../network/Response';
import * as Helper from '../../components/Helper';
export default class QRDetail extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      routes: [],
      subTasks: [],
      loading: true,
      loader: false,
      listner: 111,
      qrData: {
        userName: '',
        labName: '',
        creationDate: '',
        receptionDate: '',
        coolboxId: '',
      },
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Detail',
    headerStyle: {
      backgroundColor: colors.white,
      // alignItems: 'center',
    },
    headerLeft: () => <LeftComponent />,

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
    headerTitleAlign: 'center',
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getTasks();
    });
    this.getTasks();
  }

  async getTasks() {
    this.setState({loading: true});
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var params = {};
    params['userId'] = user.userId;
    params['sessionId'] = user.sessionId;
    params['code'] = 'CB00000007';
    // this.props.navigation.state.params.code;
    this.setState({user: user, code: params['code']});
    let response = await getBarCodeData(params);
    let totalCollectionArray = response.data.listLabReceptionSample;
    let collectionCount = totalCollectionArray.length;

    this.setState({loading: false});
    console.log(response.data.listLabReceptionSample[0]['dateTimeOfReception']);
    if (response.status == 200) {
      var qrData = this.state.qrData;
      qrData['labName'] = response.data.labName;
      qrData['userName'] = response.data.userName;
      qrData['receptionDate'] =
        response.data.listLabReceptionSample[0]['dateTimeOfReception'];
      qrData['creationDate'] = response.data.dateTimeOfCreation;
      qrData['collectionCount'] = collectionCount;
      this.setState({loading: false, qrData: qrData});
    } else if (response.status == 400) {
      Helper.showSnack(response.message);
    } else {
      Helper.showSnack(response.message);
      // this.props.navigation.navigate('Logout');
    }
  }
  navigateToPreviousScreen() {
    const { navigation } = this.props;
    navigation.goBack();
    navigation.state.params.goBackToRoute();
  }
  render() {
    if (this.state.loading) {
      return null;
    }
    return (
      
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title, {fontSize: 16}]}>
                  Lab name: {this.state.qrData.labName}
                </Text>
                <Text style={[text.title, {fontSize: 16}]}>
                  User name: {this.state.qrData.userName}
                </Text>
                <Text style={[text.title, {fontSize: 16}]}>
                  Collection Count: {this.state.qrData.collectionCount}
                </Text>
                <Text style={[text.title, {fontSize: 16}]}>
                  Creation date: {this.state.qrData.creationDate}
                </Text>
                <Text style={[text.title, {fontSize: 16}]}>
                  Reception date: {this.state.qrData.receptionDate}
                </Text>

                {/* <Text style={[text.h4, text.medium]}>
             sdf
                </Text> */}
              </View>

              <View>
                {this.state.routes.map((item, index) => {
                  return (
                    <View style={[appStyle.shadow, style.routeContainer]}>
                      <View
                        style={[
                          appStyle.rowBtw,
                          appStyle.aiFlexStart,
                          appStyle.pv10,
                        ]}>
                        <View style={[appStyle.flex1, appStyle.pr20]}>
                          {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                          <View style={[appStyle.row, appStyle.aiFlexStart]}>
                            <Image
                              source={images.locationFrom}
                              style={image.locationFromIcon}
                            />
                            <Text
                              style={[
                                text.h4,
                                text.black,
                                text.medium,
                                appStyle.ml10,
                              ]}>
                              {item.startAddress}
                            </Text>
                          </View>
                          <View
                            style={[
                              appStyle.row,
                              appStyle.mt5,
                              appStyle.aiCenter,
                            ]}>
                            <Image
                              source={images.locationTo}
                              style={image.locationToIcon}
                            />

                            <Text style={[text.h4, text.medium, appStyle.ml10]}>
                              {item.endAddress}
                            </Text>
                          </View>
                          <Text
                            style={[text.h5, appStyle.ml5, {paddingLeft: 20}]}>
                            Distance: {item.distanceText}
                          </Text>
                          <Text
                            style={[text.h5, appStyle.ml5, {paddingLeft: 20}]}>
                            Duration: {item.durationText}
                          </Text>
                          <Text
                            style={[text.h5, appStyle.ml5, {paddingLeft: 20}]}>
                            Collections: {item.collectionCount}
                          </Text>
                        </View>
                        {this.getStatusIcon(item.status)}
                      </View>
                      {/* {this.state.user.userType == 3 && (
                        <View>
                          <TouchableOpacity
                            onPress={() => this.onSelect(item, index)}
                            style={style.routeMapIcon}
                            hitSlop={hitArea.hitArea}>
                            <Image
                              source={images.route}
                              style={[image.iconMd, appStyle.asFlexEnd]}
                            />
                          </TouchableOpacity>
                        </View>
                      )} */}

                      {/* {this.state.user.userType == 2 &&
                        (item.status == 1 || item.status == 2) && ( */}
                      <View>
                        <TouchableOpacity
                          onPress={() => this.onSelect(item, index)}
                          style={style.routeMapIcon}
                          hitSlop={hitArea.hitArea}>
                          <Image
                            source={images.route}
                            style={[image.iconMd, appStyle.asFlexEnd]}
                          />
                        </TouchableOpacity>
                      </View>
                      {/* )} */}
                    </View>
                  );
                })}
                <View>
                  <View style={appStyle.mt30}>
                    <Text style={[text.title, {fontSize: 16}]}>
                      <Text style={{color: 'red'}}>*</Text>Kindly press confirm
                      that you picked this box
                    </Text>
                  </View>
                  <View style={[appStyle.rowBtw, {padding: 5}]}>
                    <ButtonDefault
                      onPress={() => this.navigateToPreviousScreen()}
                      maxWidth={'40%'}>
                      Confirm
                    </ButtonDefault>
                    <ButtonDefaultwhite
                      onPress={() =>
                        this.props.navigation.navigate('DriverTask')
                      }
                      maxWidth={'40%'}>
                      Cancel
                    </ButtonDefaultwhite>
                  </View>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
  
  );
  }
}
