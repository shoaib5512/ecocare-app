import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  Dimensions,
  ImageBackground,
  StatusBar,
} from 'react-native';
import {colors, images, screenHeight, screenWidth} from '../../config/Constant';
import {verifySession} from '../../network/Session';
export default class Splash extends React.Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true;
  }

  static navigationOptions = {
    headerShown: false,
  };

  performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('splash');
      }, 3000),
    );
  };

  async componentDidMount() {
    const nav = await verifySession();
    const data = await this.performTimeConsumingTask();
    if (data !== null) {
      this.props.navigation.navigate(nav);
    }
  }

  render() {
    return (
      <View style={styles.splashBg}>
        <View style={[styles.container]}>
          <Image style={[styles.splashImg]} source={images.logoDark} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  splashImgContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashImg: {
    resizeMode: 'contain',
    width: 250,
    height: 250,
    position: 'relative',
    zIndex: 2,
  },
  splashBg: {
    width: Dimensions.get('window').width,
    flex: 1,
    resizeMode: 'stretch',
    backgroundColor: '#fff',
  },
});
