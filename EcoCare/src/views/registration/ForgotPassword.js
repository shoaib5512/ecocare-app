import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    SafeAreaView,
    Button,
    StatusBar,
    TextInput,
    TouchableOpacity,
    CheckBox,
    Image,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    StyleSheet,
} from 'react-native';
import { colors, screenHeight, screenWidth, images } from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import LeftComponent from '../../components/headerComponent/LeftComponent';

import appStyle from '../../assets/styles/appStyle';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
    ButtonDefault,
    ButtonOutline,
    Input,
    ButtonText,
} from '../../components/Action';
export default class Login extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }

    static navigationOptions = {
        headerTintColor: colors.themeDefault,
        title: null,
        headerStyle: {
            backgroundColor: colors.white,
            alignItems: 'center',
            elevation: 0

        },
       headerLeft: () => <LeftComponent />,


        headerTitleStyle: {
            color: colors.themeDefault,
            textAlign: "center",

        },
        headerTitleAlign: 'center'
    }

    render() {
        return (
            <SafeAreaView style={[appStyle.flex1]}>

                <View style={[appStyle.mainContainer]}>
                    <KeyboardAwareScrollView contentContainerStyle={[appStyle.scrollContainerCenter]}>
                        <Image style={image.logoImg} source={images.logoDark} />

                        <View style={[appStyle.wrapper]}>

                            <Text style={[text.title]}>Reset Password</Text>
                            <Text style={[text.para]}>Enter your email address to receive{"\n"}reset
                             password instructions.</Text>
                            <Input  placeholder={'Email'} />





                            <ButtonDefault
                                onPress={() => this.props.navigation.navigate('ForgotOtp')}>
                                Continue
                            </ButtonDefault>

                        </View>
                        <View style={[appStyle.rowCenter, appStyle.mt5]}>
                            
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Login')}
                                hitSlop={appStyle.hitSlopArea}
                                style={[appStyle.mv5, appStyle.mh5]}>
                                <Text style={[text.h3, text.themeDefault, text.medium]}>
                                    Back to Login
                    </Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
