import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Linking
} from 'react-native';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import appStyle from '../../assets/styles/appStyle';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import md5 from 'md5';
import {verifySession, getSession, logout} from '../../network/Session';
import {login} from '../../network/Response';
import * as Helper from '../../components/Helper';
import {getLocationPermission} from '../../network/Permission';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      loader: false,
      params: {email: '', password: ''},
    };
  }

  static navigationOptions = {
    headerShown: false,
  };

  async componentDidMount() {
     let permission = await getLocationPermission();
     if(!permission){
      this.props.navigation.navigate('LocationPermission');
     }
     console.log(permission);
  }

  async handleSubmit() {
    var param = this.state.params;
    console.log(param.email);
    if (param.email != '') {
      if (param.password != '') {
        var userParams = {};
        userParams['email'] = param.email;
        userParams['password'] = md5(param.password);
        userParams['deviceToken'] = 121212;
        userParams['deviceType'] = Platform.OS;

        this.setState({loader: true});
        await login(userParams).then((result) => {
          this.setState({loader: false});
          if (result.status == 200) {
            saveUser('user', result.data.user);
            Helper.updateUserObject(result.data.user);
            if (result.data.user.userType == 2) {
              this.props.navigation.navigate('Home');
            } else {
              this.props.navigation.navigate('DriverTask');
            }
          } else {
            Helper.showSnack(result.message);
          }
        });
      } else {
        Helper.showSnack('Enter password.');
      }
    } else {
      Helper.showSnack('Enter Email');
    }
  }
  onChange = (value, field) => {
    var params = this.state.params;
    params[field] = value;
    this.setState({params: params});
  };

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <Image
              style={[image.logoImg, appStyle.mt40]}
              source={images.logoDark}
            />

            <View style={[appStyle.wrapper]}>
              <Text style={[text.title]}>Login to your Account</Text>

              <View style={[input.formInput]}>
                <TextInput
                  style={input.input}
                  placeholder={'Enter your email'}
                  placeholderTextColor={colors.placeholder}
                  onChangeText={(Text) => this.onChange(Text, 'email')}
                  value={this.state.params.email}></TextInput>
              </View>

              <View style={[input.formInput]}>
                <TextInput
                  style={input.input}
                  placeholder={'*******'}
                  placeholderTextColor={colors.placeholder}
                  secureTextEntry={true}
                  onChangeText={(Text) => this.onChange(Text, 'password')}
                  value={this.state.params.password}></TextInput>
              </View>
              <View
                style={[
                  appStyle.aiFlexEnd,
                  appStyle.mh5,
                  appStyle.aiFlexEnd,
                  appStyle.mtN10,
                ]}>
                {/* <ButtonText
                  maxWidth={'80%'}
                  onPress={() =>
                    this.props.navigation.navigate('ForgotPassword')
                  }
                  color={colors.black}>
                  Forget Password?
                </ButtonText> */}
              </View>

              <ButtonDefault onPress={() => this.handleSubmit()}>
                Login
              </ButtonDefault>
            </View>
            
            {/* spacer for flex space-between */}
            <View style={[appStyle.rowCenter, appStyle.mt5]}>
              <Text style={[text.h3, text.light, text.medium]}>
                For more information,
              </Text>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('http://88.85.125.77/ecocare/Privacy.html')
                }
                hitSlop={appStyle.hitSlopArea}
                style={[appStyle.mv5, appStyle.mh5]}>
                <Text style={[text.h3, text.themeDefault, text.medium]}>
                  Privacy Policy
                </Text>
              </TouchableOpacity>
            </View>
            <View/>
            {/* end spacer */}
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

async function saveUser(user, data) {
  try {
    await AsyncStorage.setItem(user, JSON.stringify(data));
    console.log('data SAVED', JSON.stringify(data));
  } catch (error) {}
}
