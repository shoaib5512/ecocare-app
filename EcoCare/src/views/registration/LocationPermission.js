import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Linking,
} from 'react-native';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import appStyle from '../../assets/styles/appStyle';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import md5 from 'md5';
import {verifySession, getSession, logout} from '../../network/Session';
import {login} from '../../network/Response';

import * as Helper from '../../components/Helper';
import {getLocationPermission} from '../../network/Permission';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
export default class LocationPermission extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: true,
    };
  }

  static navigationOptions = {
    headerShown: false,
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getDetail();
    });
    this.getDetail();
  }

  async getDetail() {
    var userSession = await AsyncStorage.getItem('user');
    var locationPermission = await AsyncStorage.getItem('locationPermission');
    locationPermission = JSON.parse(locationPermission);
    var nav = 'Login';
    if (locationPermission != null) {
      this.setState({loading: false});
      this.props.navigation.navigate('Splash');
    } else {
      this.setState({loading: false});
    }
  }

  async handleSubmit() {
    var data = {
      permission: true,
    };
    // await AsyncStorage.removeItem('locationPermission');
    await AsyncStorage.setItem('locationPermission', JSON.stringify(data));
    this.getDetail();
    // this.props.navigation.navigate('Splash');
  }

  render() {
    if (this.state.loading) {
      return false;
    }
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <Image style={[image.logoImg]} source={images.logoDark} />
            <View style={[appStyle.wrapper]}>
              <Text style={[text.hl, text.black, text.medium]}>
                Location Permission
              </Text>
              <View style={[appStyle.mv5]} />
              <Text style={[text.h3, text.darkGrey, text.meddium]}>
                E-Delivery collects location data to enable your device location
                to track your live location updates, even when the app is closed
                or not in use,
              </Text>
              <View style={[appStyle.mv5]} />
              <Text style={[text.h3, text.darkGrey, text.meddium]}>
                To use this application you must have to accept and allow to
                enable and track your live location updates.
              </Text>
              <ButtonDefault onPress={() => this.handleSubmit()}>
                Accept
              </ButtonDefault>
            </View>
            {/* spacer for flex space-between */}

            <View style={[appStyle.rowCenter, appStyle.mt5]}>
              <Text style={[text.h3, text.light, text.medium]}>
                For more information,
              </Text>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('http://88.85.125.77/ecocare/Privacy.html')
                }
                hitSlop={appStyle.hitSlopArea}
                style={[appStyle.mv5, appStyle.mh5]}>
                <Text style={[text.h3, text.themeDefault, text.medium]}>
                  Privacy Policy
                </Text>
              </TouchableOpacity>
            </View>
            <View />
            {/* end spacer */}
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

async function saveUser(user, data) {
  try {
    await AsyncStorage.setItem(user, JSON.stringify(data));
    console.log('data SAVED', JSON.stringify(data));
  } catch (error) {}
}
