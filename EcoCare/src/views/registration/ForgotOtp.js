import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    SafeAreaView,
    Button,
    StatusBar,
    TextInput,
    TouchableOpacity,
    CheckBox,
    Image,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    StyleSheet,
} from 'react-native';
import { colors, screenHeight, screenWidth, images } from '../../config/Constant';

import appStyle from '../../assets/styles/appStyle';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import style from '../../assets/styles/style';

import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import LeftComponent from '../../components/headerComponent/LeftComponent';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

import {
    ButtonDefault,
    ButtonOutline,
    Input,
    ButtonText,
} from '../../components/Action';
import OTPInputView from '@twotalltotems/react-native-otp-input'
var otpRequired;
var codeEntered;

export default class ForgotPassword extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            email: '',
            otp: '',
            codeEntered: '',
            otpRequired: '',
            focusInputEmail: colors.lightGrey,
        };
    }

    static navigationOptions = {
        headerTintColor: colors.themeDefault,
        title: null,
        headerStyle: {
            backgroundColor: colors.white,
            alignItems: 'center',
            elevation: 0

        },
       headerLeft: () => <LeftComponent />,


        headerTitleStyle: {
            color: colors.themeDefault,
            textAlign: "center",

        },
        headerTitleAlign: 'center'
    }
   


    handleSubmit() {
        // console.log('-----------code reuqired---------',otpRequired);
        console.log('-------code saved-------', this.state.otp);
        if (this.state.otp == this.state.otpRequired) {
            this.props.navigation.navigate('ResetPassword', {
                data: this.state.otp,
            });
            console.log('OTP ENtered correct', this.state.otp)
        } else {
            console.log('OTP ENtered INcorrect', this.state.otp)
        }
    }


    resendOtp() {
        var RandomNumber = Math.floor(Math.random() * 88888) + 10000;

        this.setState({

            otpRequired: RandomNumber,

        })
        console.warn('your otp code is ---------', RandomNumber);
    }

    render() {
        return (
            <SafeAreaView style={[appStyle.safeContainer]}>
                <View style={[appStyle.mainContainer]}>

                <KeyboardAwareScrollView contentContainerStyle={[appStyle.scrollContainerCenter]}>
                <Image style={image.logoImg} source={images.logoDark} />

                    <View style={[appStyle.wrapper, appStyle.mtN10]}>
                        <View style={[appStyle.mb20]}>
                        <Text style={[text.title]}>Verify</Text>
                        <Text style={[text.para]}>We have sent a 5-digit code to your email. {"\n"}Enter it here to reset your password.</Text>
                        </View>
                   
                        <View style={[appStyle.mb20]}>
                            <OTPInputView
                                style={[appStyle.w100, appStyle.asCenter, { height: 50 }]}
                                pinCount={5}
                                autoFocusOnLoad
                                codeInputFieldStyle={style.otp}
                                codeInputHighlightStyle={style.otpHighlighted}
                                onCodeFilled={codeEntered => {
                                    this.setState({ otp: codeEntered });
                                    console.log(`Code is ${codeEntered}, you are good to go!`);
                                }}
                            />
                        </View>
                        <ButtonDefault
                            onPress={() => this.props.navigation.navigate('ForgotOtp')}>
                            Continue
                        </ButtonDefault>
                           
                    </View>
                   
                    {/*spacer */}
                    <View/>
                    {/*spacer end*/}

                </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        );
    }
}
