import React from 'react';
import {
  View,
  Text,
  AsyncStorage,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
} from 'react-native';

import {logout} from '../../network/Session';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';

export default class Logout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
    };
    console.disableYellowBox = true;
  }
  static navigationOptions = {
    header: null,
  };

  async componentDidMount() {
    let nav = await logout();
    this.setState({loader: false});
    this.props.navigation.navigate('Login');
  }

  render() {
    return (
     <Text/>
    );
  }
}
