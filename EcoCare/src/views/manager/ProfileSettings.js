import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
} from 'react-native';
import { colors, screenHeight, screenWidth, images } from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import ImagePicker from 'react-native-image-picker';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';

import LeftComponentWhite from '../../components/headerComponent/LeftComponentWhite';

export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: "name",
      photo: images.dummyUser1,
    };
  }

  static navigationOptions = {
    headerTintColor: colors.white,
    title: 'Profile',
    headerStyle: {
      backgroundColor: colors.themeDefault,
      elevation: 0

    },
    headerLeft: () => <LeftComponentWhite />,


    headerTitleStyle: {
      color: colors.white,
      textAlign: "center",
    },
    headerTitleAlign: 'center'

  }


  handleChoosePhoto = () => {
    const options = {
      title: 'Take Image From',
      StorageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response) {
        console.log(response);
        this.setState({ photo: response });
      } else if (response.didCancel) {
        console.log('User Cancelled Image Picker');
      } else if (response.error) {
        console.log('Image Picker Error', response.error);
      }
    });
  };

  render() {
    const { photo } = this.state;

    return (
      <SafeAreaView style={[appStyle.flex1]}>

        <View style={[appStyle.mainContainerSettings]}>
          <View
            style={style.settingsHeader}
          >
            <TouchableOpacity 
            onPress={this.handleChoosePhoto}
            style={{position:'relative'}}>
            <View 
            style={[
             ]}
            >
              {photo && (
            <Image source={this.state.photo} style={
              image.profile
            } />
            )}
            </View>
            
            
           
             <Image source={images.plus} style={[image.icon,style.addImgIcon]}/>
            </TouchableOpacity>
           
            <View style={[appStyle.pl15]}>
              <Text style={[text.h1, text.white, text.medium]}>John Doe</Text>
              <Text style={[text.h3, text.white, text.medium]}>+143 566 666</Text>
            </View>

          </View>
          <KeyboardAwareScrollView contentContainerStyle={[appStyle.scrollContainerCenter]}>

            <View style={[appStyle.wrapper]}>
            <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>Basic Information</Text>
              </View>
              <Input  placeholder={'First Name'} />
              <Input  placeholder={'Last Name'} />
              <Input  placeholder={'Phone'} />

              <ButtonDefault>
                Update
              </ButtonDefault>

              

             

             

             

             
              <View>






              </View>







            </View>

          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
