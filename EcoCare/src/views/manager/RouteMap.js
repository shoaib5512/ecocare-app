import React, { Component, createRef } from 'react';
import {
    Text,
    View,
    ScrollView,
    SafeAreaView,
    Button,
    StatusBar,
    TextInput,
    TouchableOpacity,
    CheckBox,
    Image,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    StyleSheet,
} from 'react-native';
import { colors, screenHeight, screenWidth, images, hitArea } from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { RouteButton } from '../../components/headerComponent/RightComponent';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
    ButtonDefault,
    ButtonOutline,
    Input,
    ButtonText,
} from '../../components/Action';
import ActionSheet from "react-native-actions-sheet";
import LeftComponent from '../../components/headerComponent/LeftComponent';


const actionSheetRef = createRef();
export default class Login extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            value: "name"

        };
    }
    // componentDidMount() {
    //     actionSheetRef.current?.setModalVisible(true);
    // }
  

    

      static navigationOptions = {
        headerTintColor: colors.themeDefault,
        title: 'Route',
        headerStyle: {
            backgroundColor: colors.white,
            // alignItems: 'center'

        },
        headerRight: () => <RouteButton 
          onPress={() => {
            actionSheetRef.current?.setModalVisible(true);
          }}
          />,
       headerLeft: () => <LeftComponent />,


        headerTitleStyle: {
            color: colors.themeDefault,
            textAlign: "center",

        },
        headerTitleAlign: 'center'
    }

      

    render() {
        let actionSheet;
        return (
            <SafeAreaView style={[appStyle.flex1]}>

                <View style={[appStyle.mainContainer]}>
                    <KeyboardAwareScrollView contentContainerStyle={[appStyle.scrollContainerCenter]}>

                        <View style={[appStyle.wrapper]}>
                            <View style={[appStyle.mb10]}>
                                <Text style={[text.title]}>Hauptstrasse - Dorfstrasse</Text>
                            </View>

                            <View>


                                <ActionSheet ref={actionSheetRef}

                                    containerStyle={style.actionSheet}
                                    style={style.actionSheet}
                                    animated={true}
                                    headerAlwaysVisible={true}
                                    bounceOnOpen={true}
                                    overlayColor={colors.black}
                                    defaultOverlayOpacity={0.45}
                                    elevation={10}
                                    footerHeight={2}
                                    statusBarTranslucent={false}
                                    indicatorColor={'#C6C8CB'}
                                // closeOnPressBack={false}
                                >
                                    <View style={[style.actionSheetContainer, appStyle.p20]}>
                                        <View
                                            style={[style.routeContainer, appStyle.p0]}>

                                            <View style={[appStyle.rowBtw, appStyle.aiFlexStart, appStyle.pv10]}>
                                                <View style={[appStyle.flex1, appStyle.pr20]}>
                                                    <Image source={images.locationLine}
                                                        style={{ position: 'absolute', left: 7 }}
                                                    />
                                                    <View style={[appStyle.row, appStyle.aiFlexStart]}>
                                                        <Image source={images.locationFrom}
                                                            style={image.locationFromIcon}
                                                        />
                                                        <Text style={[text.h4, text.black, text.medium, appStyle.ml10]}>
                                                            660 N. Capitol St. NW Washington
                                               </Text>
                                                    </View>
                                                    <View style={[appStyle.row, appStyle.mt5, appStyle.aiCenter]}>
                                                        <Image source={images.locationTo}
                                                            style={image.locationToIcon}
                                                        />
                                                        <Text style={[text.h4, text.medium, appStyle.ml10]}>
                                                            Great North Road - Canal Road
                                                 </Text>
                                                    </View>

                                                </View>


                                            </View>


                                        </View>



                                        <ScrollView

                                        >
                                            <View>
                                                <View style={[appStyle.row, appStyle.pb15]}>
                                                    <Image source={images.current} style={image.icon} />
                                                    <Text style={[text.h3, appStyle.ml10]}>On the way</Text>

                                                </View>

                                                <View style={[appStyle.row, appStyle.pb15]}>
                                                    <Image source={images.upcoming} style={image.icon} />
                                                    <Text style={[text.h3, appStyle.ml10]}>Upcoming</Text>

                                                </View>

                                                <View style={[appStyle.row, appStyle.pb15]}>
                                                    <Image source={images.complete} style={image.icon} />
                                                    <Text style={[text.h3, appStyle.ml10]}>Completed</Text>

                                                </View>


                                            </View>
                                        </ScrollView>

                                        
                                        <View style={[appStyle.row, appStyle.aiCenter]}>
                                            <Text style={[text.h3, appStyle.mr20]}>Distance: <Text style={[text.h3, text.greyDark, appStyle.pl5]}>5km</Text></Text>
                                            <Text style={[text.h3, appStyle.mr20]}>Collections: <Text style={[text.h3, text.greyDark, appStyle.pl5]}>10</Text></Text>
                                        </View>
                                        <View>
                                            <ButtonDefault>End</ButtonDefault>
                                        </View>
                                    </View>
                                </ActionSheet>


                            </View>







                        </View>

                    </KeyboardAwareScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
