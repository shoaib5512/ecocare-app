import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
} from 'react-native';
import { colors, screenHeight, screenWidth, images } from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: "name"

    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Tasks',
    headerStyle: {
      backgroundColor: colors.white,

    },
    headerTitleAlign: 'center',

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: "center",
    },
  }

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>

        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView contentContainerStyle={[appStyle.scrollContainerCenter]}>

            <View style={[appStyle.wrapper]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>Your Tasks</Text>
              </View>

              <View>

                <View 
                style={[appStyle.shadow,style.taskContainer]}>
                  <Text style={[text.h2]}>Hauptstrasse - Dorfs</Text>
                  <Text style={[text.h4]}>20-08-2020</Text>
                  <Text style={[text.para,text.black,appStyle.mt10]}>The greatest disease in the West today is not TB or leprosy it is being unwanted, unloved, and uncared
                     The greatest disease in the West today is not TB leprosy it is being unwanted, unloved, and uncared</Text>
                  <View style={[appStyle.rowBtw,appStyle.mt10]}>
                    <Text style={[text.h4,text.black,text.medium]}>
                      Total route : 8
                    </Text>
                    <TouchableOpacity 
                    onPress={()=> this.props.navigation.navigate('Route')}
                    style={[button.btnRoute]}
                    >
                      <Text style={[text.h4,text.white]}>Check Route</Text>
                    </TouchableOpacity>
                  </View>
                </View>


                <View 
                style={[appStyle.shadow,style.taskContainer]}>
                  <Text style={[text.h2]}>Hauptstrasse - Dorfstrasse</Text>
                  <Text style={[text.h4]}>20-08-2020</Text>
                  <Text style={[text.para,text.black,appStyle.mt10]}>The greatest disease in the West today is not TB or leprosy it is being unwanted, unloved, and uncared
                     The greatest disease in the West today is not TB leprosy it is being unwanted, unloved, and uncared</Text>
                  <View style={[appStyle.rowBtw,appStyle.mt10]}>
                    <Text style={[text.h4,text.black,text.medium]}>
                      Total route : 8
                    </Text>
                    <TouchableOpacity 
                    style={[button.btnRoute]}
                    >
                      <Text style={[text.h4,text.white]}>Check Route</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>







            </View>

          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
