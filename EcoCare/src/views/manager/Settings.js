import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
} from 'react-native';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Settings',
    headerStyle: {
      backgroundColor: colors.themeDefault,
      elevation: 0,
    },

    headerTitleStyle: {
      color: colors.white,
      textAlign: 'center',
      flex: 1,
    },
  };

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainerSettings]}>
          <View style={style.settingsHeader}>
            <Image source={images.dummyUser1} style={image.profile} />
            <View style={[appStyle.pl15]}>
              <Text style={[text.h1, text.white, text.medium]}>John Doe</Text>
              <Text style={[text.h3, text.white, text.medium]}>
                +143 566 666
              </Text>
            </View>
          </View>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <TouchableOpacity
                style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}
                onPress={() =>
                  this.props.navigation.navigate('ProfileSettings')
                }>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image
                    source={images.user}
                    style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Profile</Text>
                </View>

                <Image source={images.arrow} style={image.iconSettingsArrow} />
              </TouchableOpacity>

              <View style={style.borderBottom} />

              {/* <TouchableOpacity style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image source={images.bell} style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Notification</Text>
                </View>

                <Image source={images.arrow} style={image.iconSettingsArrow} />
              </TouchableOpacity>

              <View
                style={style.borderBottom} /> */}

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Logout')}
                style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image
                    source={images.logout}
                    style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Logout</Text>
                </View>

                <Image source={images.arrow} style={image.iconSettingsArrow} />
              </TouchableOpacity>

              <View></View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
