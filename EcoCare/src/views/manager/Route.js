import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    SafeAreaView,
    Button,
    StatusBar,
    TextInput,
    TouchableOpacity,
    CheckBox,
    Image,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    StyleSheet,
} from 'react-native';
import { colors, screenHeight, screenWidth, images, hitArea } from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import LeftComponent from '../../components/headerComponent/LeftComponent';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
    ButtonDefault,
    ButtonOutline,
    Input,
    ButtonText,
} from '../../components/Action';
export default class Login extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            value: "name"

        };
    }

    static navigationOptions = {
        headerTintColor: colors.themeDefault,
        title: 'Route',
        headerStyle: {
            backgroundColor: colors.white,
            // alignItems: 'center'

        },
       headerLeft: () => <LeftComponent />,


        headerTitleStyle: {
            color: colors.themeDefault,
            textAlign: "center",

        },
        headerTitleAlign: 'center'
    }

    render() {
        return (
            <SafeAreaView style={[appStyle.flex1]}>

                <View style={[appStyle.mainContainer]}>
                    <KeyboardAwareScrollView contentContainerStyle={[appStyle.scrollContainerCenter]}>

                        <View style={[appStyle.wrapper]}>
                            <View style={[appStyle.mb10]}>
                                <Text style={[text.title]}>Hauptstrasse - Dorfstrasse</Text>
                            </View>

                            <View>


                                <View
                                    style={[appStyle.shadow, style.routeContainer]}>

                                    <View style={[appStyle.rowBtw, appStyle.aiFlexStart, appStyle.pv10]}>
                                        <View style={[appStyle.flex1,appStyle.pr20]}>
                                            {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                                                <Image source={images.locationFrom}
                                                   style={image.locationFromIcon}
                                                />
                                                <Text style={[text.h4, text.black, text.medium, appStyle.ml10]}>
                                                    660 N. Capitol St. NW Washington
                                               </Text>
                                            </View>
                                            <View style={[appStyle.row, appStyle.mt5, appStyle.aiCenter]}>
                                                <Image source={images.locationTo}
                                                    style={image.locationToIcon}
                                                />
                                                <Text style={[text.h4, text.medium, appStyle.ml10]}>
                                                    Great North Road - Canal Road
                                                 </Text>
                                            </View>

                                        </View>
                                                
                                        <View
                                            style={[appStyle.row,appStyle.aiCenter]}
                                        >
                                            <Image source={images.complete} style={[image.iconSm]} />
                                            <Text style={[text.h4,appStyle.ml5]}>Completed</Text>
                                        </View>
                                    </View>
                                    
                                    <TouchableOpacity 
                                    style={style.routeMapIcon}
                                    hitSlop={hitArea.hitArea}
                                    >
                                    <Image
                                    source={images.route}
                                    style={[image.iconMd,appStyle.asFlexEnd]}
                                    />
                                    </TouchableOpacity>
                                </View>





                                <View
                                    style={[appStyle.shadow, style.routeContainer]}>

                                    <View style={[appStyle.rowBtw, appStyle.aiFlexStart, appStyle.pv10]}>
                                        <View style={[appStyle.flex1,appStyle.pr20]}>
                                            {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                                                <Image source={images.locationFrom}
                                                   style={image.locationFromIcon}
                                                />
                                                <Text style={[text.h4, text.black, text.medium, appStyle.ml10]}>
                                                    660 N. Capitol St. NW Washington
                                               </Text>
                                            </View>
                                            <View style={[appStyle.row, appStyle.mt5, appStyle.aiCenter]}>
                                                <Image source={images.locationTo}
                                                    style={image.locationToIcon}
                                                />
                                                <Text style={[text.h4, text.medium, appStyle.ml10]}>
                                                    Great North Road - Canal Road
                                                 </Text>
                                            </View>

                                        </View>
                                                
                                        <View
                                            style={[appStyle.row,appStyle.aiCenter]}
                                        >
                                            <Image source={images.complete} style={[image.iconSm]} />
                                            <Text style={[text.h4,appStyle.ml5]}>Completed</Text>
                                        </View>
                                    </View>
                                    
                                    <TouchableOpacity 
                                    style={style.routeMapIcon}
                                    hitSlop={hitArea.hitArea}
                                    onPress={()=> this.props.navigation.navigate('RouteMap')}

                                    >
                                    <Image
                                    source={images.route}
                                    style={[image.iconMd,appStyle.asFlexEnd]}
                                    />
                                    </TouchableOpacity>
                                </View>






                                <View
                                    style={[appStyle.shadow, style.routeContainer]}>

                                    <View style={[appStyle.rowBtw, appStyle.aiFlexStart, appStyle.pv10]}>
                                        <View style={[appStyle.flex1,appStyle.pr20]}>
                                            {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                                                <Image source={images.locationFrom}
                                                   style={image.locationFromIcon}
                                                />
                                                <Text style={[text.h4, text.black, text.medium, appStyle.ml10]}>
                                                    660 N. Capitol St. NW Washington
                                               </Text>
                                            </View>
                                            <View style={[appStyle.row, appStyle.mt5, appStyle.aiCenter]}>
                                                <Image source={images.locationTo}
                                                    style={image.locationToIcon}
                                                />
                                                <Text style={[text.h4, text.medium, appStyle.ml10]}>
                                                    Great North Road - Canal Road
                                                 </Text>
                                            </View>

                                        </View>
                                                
                                        <View
                                            style={[appStyle.row,appStyle.aiCenter]}
                                        >
                                            <Image source={images.complete} style={[image.iconSm]} />
                                            <Text style={[text.h4,appStyle.ml5]}>Completed</Text>
                                        </View>
                                    </View>
                                    
                                    <TouchableOpacity 
                                    style={style.routeMapIcon}
                                    hitSlop={hitArea.hitArea}
                                    onPress={()=> this.props.navigation.navigate('RouteMap')}

                                    >
                                    <Image
                                    source={images.route}
                                    style={[image.iconMd,appStyle.asFlexEnd]}
                                    />
                                    </TouchableOpacity>
                                </View>







                                <View
                                    style={[appStyle.shadow, style.routeContainer]}>

                                    <View style={[appStyle.rowBtw, appStyle.aiFlexStart, appStyle.pv10]}>
                                        <View style={[appStyle.flex1,appStyle.pr20]}>
                                            {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                                                <Image source={images.locationFrom}
                                                   style={image.locationFromIcon}
                                                />
                                                <Text style={[text.h4, text.black, text.medium, appStyle.ml10]}>
                                                    660 N. Capitol St. NW Washington
                                               </Text>
                                            </View>
                                            <View style={[appStyle.row, appStyle.mt5, appStyle.aiCenter]}>
                                                <Image source={images.locationTo}
                                                    style={image.locationToIcon}
                                                />
                                                <Text style={[text.h4, text.medium, appStyle.ml10]}>
                                                    Great North Road - Canal Road
                                                 </Text>
                                            </View>

                                        </View>
                                                
                                        <View
                                            style={[appStyle.row,appStyle.aiCenter]}
                                        >
                                            <Image source={images.complete} style={[image.iconSm]} />
                                            <Text style={[text.h4,appStyle.ml5]}>Completed</Text>
                                        </View>
                                    </View>
                                    
                                    <TouchableOpacity 
                                    style={style.routeMapIcon}
                                    hitSlop={hitArea.hitArea}
                                    onPress={()=> this.props.navigation.navigate('RouteMap')}
                                    >
                                    <Image
                                    source={images.route}
                                    style={[image.iconMd,appStyle.asFlexEnd]}
                                    />
                                    </TouchableOpacity>
                                </View>

                            </View>







                        </View>

                    </KeyboardAwareScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
