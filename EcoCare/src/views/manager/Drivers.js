import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  hitArea,
} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
var firebase = require('firebase');
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {getAllTask} from '../../network/Response';
import * as Helper from '../../components/Helper';
export default class Drivers extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      tabLiFirstBorder: colors.themeDefault,
      tabLiSecondBorder: colors.white,
      tabLiThirdBorder: colors.white,

      tabLiFirstColor: colors.themeDefault,
      tabLiSecondColor: colors.black,
      tabLiThirdColor: colors.black,

      tabLiFirst: 'flex',
      tabLiSecond: 'none',
      tabLiThird: 'none',
      drivers: [],
      upcoming: [],
      completed: [],
      loader: false,
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Drivers',
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTitleAlign: 'center',

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getDrivers();
    });
    this.getDrivers();
  }

  async getDrivers() {
    this.setState({loader: true});

    firebase
      .database()
      .ref('Users')
      //   .child(taskId)
      .on('value', (drivers) => {
        this.setState({loader: false});
        var driversObj = [];
        if (drivers.val() != null || drivers.val() != undefined) {
          Object.values(drivers.val()).forEach((obj) => {
            if (obj.userType == 3) {
              driversObj.push(obj);
            }
          });
          this.setState({drivers: driversObj});
        }
      });
  }

  async onSelect(id) {
    console.log(id);
    this.props.navigation.navigate('ManagerTaskRoutes', {
      taskId: id,
    });
  }

  getStatusIcon(status) {

    if (status == 1) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.complete} style={[image.iconSm]} />
      <Text style={[text.h4, appStyle.ml5]}>Online</Text>
        </View>
      );
    } else if (status == 0) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.upcoming} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>Offline</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer, appStyle.pt0]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <View>
                {this.state.drivers.length > 0 ? (
                  this.state.drivers.map((item, index) => {
                    return (
                      <View style={[appStyle.shadow, style.routeContainer]}>
                        <View
                          style={[
                            appStyle.rowBtw,
                            appStyle.aiFlexStart,
                            appStyle.pv10,
                          ]}>
                          <View style={[appStyle.flex1, appStyle.pr15]}>
                            <View
                              style={[
                                appStyle.row,
                                appStyle.aiFlexStart,
                                appStyle.pb15,
                              ]}>
                              <Image
                                source={{uri: item.image}}
                                style={image.profileSm}
                              />
                              <View style={[appStyle.pl5]}>
                                <Text style={[text.h3, text.medium]}>
                                  {item.firstName + ' ' + item.lastName}
                                </Text>
                                <Text style={[text.h4, text.regular]}>
                                  {item.vehicleNo}
                                </Text>
                                <Text style={[text.h4, text.regular]}>
                                  {item.phone}
                                </Text>
                              </View>
                            </View>
                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                              <Text
                                style={[
                                  text.h4,
                                  text.black,
                                  text.medium,
                                  appStyle.ml10,
                                ]}>
                                {item.title}
                              </Text>
                            </View>
                          </View>

                          {this.getStatusIcon(item.isOnline)}
                        </View>

                        {/* <View
                          style={[appStyle.wrapContainer, appStyle.jcFlexEnd]}>
                          <TouchableOpacity
                            onPress={() => this.onSelect(item.userId)}
                            style={[button.btnRoute]}>
                            <Text style={[text.h4, text.white]}>
                              View Tasks
                            </Text>
                          </TouchableOpacity>
                        </View> */}
                      </View>
                    );
                  })
                ) : (
                  <View>
                    <View style={[style.margin40]} />
                    <Text
                      style={[
                        text.para,
                        text.black,
                        appStyle.mt10,
                        text.center,
                      ]}>
                      No drivers found.
                    </Text>
                  </View>
                )}
              </View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
