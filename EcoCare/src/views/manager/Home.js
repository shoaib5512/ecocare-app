import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  hitArea,
} from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import { getAllTask } from '../../network/Response';
import * as Helper from '../../components/Helper';
export default class Home extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      tabLiFirstBorder: colors.themeDefault,
      tabLiSecondBorder: colors.white,
      tabLiThirdBorder: colors.white,

      tabLiFirstColor: colors.themeDefault,
      tabLiSecondColor: colors.black,
      tabLiThirdColor: colors.black,

      tabLiFirst: 'flex',
      tabLiSecond: 'none',
      tabLiThird: 'none',
      inprogress: [],
      upcoming: [],
      completed: [],
      loader: false,
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Home',
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTitleAlign: 'center',

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
  };

  tabLiFirst = () => {
    if ((this.state.tabLiFirst = 'none')) {
      this.setState({
        tabLiFirstBorder: colors.themeDefault,
        tabLiSecondBorder: colors.white,
        tabLiThirdBorder: colors.white,

        tabLiFirstColor: colors.themeDefault,
        tabLiSecondColor: colors.black,
        tabLiThirdColor: colors.black,

        tabLiFirst: 'flex',
        tabLiSecond: 'none',
        tabLiThird: 'none',
      });
    } else {
      this.setState({
        tabLiFirst: 'flex',
      });
    }
  };

  tabLiSecond = () => {
    if ((this.state.tabLiSecond = 'none')) {
      this.setState({
        tabLiFirstBorder: colors.white,
        tabLiSecondBorder: colors.themeDefault,
        tabLiThirdBorder: colors.white,

        tabLiFirstColor: colors.black,
        tabLiSecondColor: colors.themeDefault,
        tabLiThirdColor: colors.black,

        tabLiFirst: 'none',
        tabLiSecond: 'flex',
        tabLiThird: 'none',
      });
    } else {
      this.setState({
        tabLiSecond: 'flex',
      });
    }
  };

  tabLiThird = () => {
    if ((this.state.tabLiThird = 'none')) {
      this.setState({
        tabLiFirstBorder: colors.white,
        tabLiSecondBorder: colors.white,
        tabLiThirdBorder: colors.themeDefault,

        tabLiFirstColor: colors.black,
        tabLiSecondColor: colors.black,
        tabLiThirdColor: colors.themeDefault,

        tabLiFirst: 'none',
        tabLiSecond: 'none',
        tabLiThird: 'flex',
      });
    } else {
      this.setState({
        tabLiThird: 'flex',
      });
    }
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getTasks();
    });
    this.getTasks();
  }

  async getTasks() {
    this.setState({ loader: true });
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var params = {};
    params['userId'] = user.userId;
    params['sessionId'] = user.sessionId;
    // params['offset'] = this.state.offset;
    this.setState({ user: user });
    console.warn(params);
    let response = await getAllTask(params);
    this.setState({ loader: false });
    if (response.status == 200) {
      this.setState({
        inprogress: response.data.inprogress,
        upcoming: response.data.upcoming,
        completed: response.data.completed,
        offset: this.state.offset + 1,
      });
    } else if (response.status == 400) {
      Helper.showSnack(response.message);
    } else {
      Helper.showSnack(response.message);
      this.props.navigation.navigate('Logout');
    }
  }

  async onSelect(id) {
    console.log(id);
    this.props.navigation.navigate('ManagerTaskRoutes', {
      taskId: id,
    });
  }

  getStatusIcon(status) {
    if (status == 0) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.upcoming} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>Upcoming</Text>
        </View>
      );
    } else if (status == 1) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.current} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>On the way</Text>
        </View>
      );
    } else if (status == 2) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.complete} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>Completed</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer, appStyle.pt0]}>
          <View style={[appStyle.rowBtw, appStyle.shadow, appStyle.bgWhite]}>
            <TouchableOpacity
              style={[
                style.tabsLi,
                {
                  borderBottomColor: this.state.tabLiFirstBorder,
                },
              ]}
              onPress={() => this.tabLiFirst()}>
              <Text
                style={[
                  text.h4,
                  text.medium,
                  { color: this.state.tabLiFirstColor },
                ]}>
                In Progress ({this.state.inprogress.length})
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                style.tabsLi,
                {
                  borderBottomColor: this.state.tabLiSecondBorder,
                },
              ]}
              onPress={() => this.tabLiSecond()}>
              <Text
                style={[
                  text.h4,
                  text.medium,
                  { color: this.state.tabLiSecondColor },
                ]}>
                Complete ({this.state.completed.length})
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                style.tabsLi,
                {
                  borderBottomColor: this.state.tabLiThirdBorder,
                },
              ]}
              onPress={() => this.tabLiThird()}>
              <Text
                style={[
                  text.h4,
                  text.medium,
                  { color: this.state.tabLiThirdColor },
                ]}>
                Upcoming ({this.state.upcoming.length})
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={[appStyle.scrollContainerCenter]}
          >
            <View style={[appStyle.wrapper, appStyle.pb0, appStyle.pt0]}>

              <View
                style={{
                  display: this.state.tabLiFirst,
                  position: 'relative',
                  height: '100%',
                }}>
                
                <View
                  style={{
                    // height: screenHeight.height100 - 220
                    paddingBottom:40
                  }}
                >
                   <ScrollView
                    contentContainerStyle={{
                      flexGrow: 1,
                      paddingTop: 20,
                    }}
                    showsVerticalScrollIndicator={false}
                  >
                {this.state.inprogress.length > 0 ? (
                  this.state.inprogress.map((item, index) => {
                    return (
                      <View style={[appStyle.shadow, style.routeContainer]}>

                        <View
                          style={[
                            appStyle.rowBtw,
                            appStyle.aiFlexStart,
                            appStyle.pv10,
                          ]}>
                          <View style={[appStyle.flex1, appStyle.pr15]}>
                            {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                            <View
                              style={[
                                appStyle.row,
                                appStyle.aiFlexStart,
                                appStyle.pb15,
                              ]}>
                              <Image
                                source={{ uri: item.driverImage }}
                                style={image.profileSm}
                              />
                              <View style={[appStyle.pl5]}>
                                <Text style={[text.h3, text.medium]}>
                                  {item.firstName + ' ' + item.lastName}
                                </Text>
                                <Text style={[text.h4, text.regular]}>
                                  {item.vehicleNo}
                                </Text>
                              </View>
                            </View>
                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                              {/* <Image
                                source={images.locationFrom}
                                style={image.locationFromIcon}
                              /> */}
                              <Text
                                style={[
                                  text.h4,
                                  text.black,
                                  text.medium,
                                  appStyle.ml10,
                                ]}>
                                {item.title}
                              </Text>
                            </View>
                            {/* <View
                              style={[
                                appStyle.row,
                                appStyle.mt5,
                                appStyle.aiCenter,
                              ]}>
                              <Image
                                source={images.locationTo}
                                style={image.locationToIcon}
                              />
                              <Text
                                style={[text.h4, text.medium, appStyle.ml10]}>
                                Great North Road - Canal Road,
                              </Text>
                            </View> */}
                          </View>

                          {this.getStatusIcon(item.status)}
                        </View>

                        {/* <TouchableOpacity
                          style={style.routeMapIcon}
                          hitSlop={hitArea.hitArea}>
                          <Image
                            source={images.route}
                            style={[image.iconMd, appStyle.asFlexEnd]}
                          />
                        </TouchableOpacity> */}
                        <View style={[appStyle.wrapContainer, appStyle.jcFlexEnd]}>
                          <TouchableOpacity
                            onPress={() => this.onSelect(item.id)}
                            style={[button.btnRoute]}>
                            <Text style={[text.h4, text.white]}>Check Route</Text>
                          </TouchableOpacity>
                        </View>

                      </View>
                    );
                  })
                ) : (
                    <View>
                      <View style={[style.margin40]} />
                      <Text
                        style={[
                          text.para,
                          text.black,
                          appStyle.mt10,
                          text.center,
                        ]}>
                        No tasks found.
                    </Text>
                    </View>
                  )}
                  </ScrollView>
              </View>
                 {/* load more btn */}
                 {/* <View
                  style={style.btnLoadMoreContainer}
                >
                  <TouchableOpacity>
                    <Text
                      style={[text.text16, text.medium, text.themeDefault, text.center]}
                    >Load more...</Text>
                  </TouchableOpacity>
                </View> */}
                {/* load more btn end */}
              </View>

              <View
                style={{
                  display: this.state.tabLiSecond,
                  position: 'relative',
                  height: '100%',
                }}
              >
                <View
                  style={{
                    // height: screenHeight.height100 - 220
                    paddingBottom:40
                  }}
                >
                  <ScrollView
                    contentContainerStyle={{
                      flexGrow: 1,
                      paddingTop: 20,
                    }}
                    showsVerticalScrollIndicator={false}
                  >




                    {this.state.completed.length > 0 ? (
                      this.state.completed.map((item, index) => {
                        return (
                          <View style={[appStyle.shadow, style.routeContainer]}>
                            <View
                              style={[
                                appStyle.rowBtw,
                                appStyle.aiFlexStart,
                                appStyle.pv10,
                              ]}>
                              <View style={[appStyle.flex1, appStyle.pr15]}>
                                {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                                <View
                                  style={[
                                    appStyle.row,
                                    appStyle.aiFlexStart,
                                    appStyle.pb15,
                                  ]}>
                                  <Image
                                    source={{ uri: item.driverImage }}
                                    style={image.profileSm}
                                  />
                                  <View style={[appStyle.pl5]}>
                                    <Text style={[text.h3, text.medium]}>
                                      {item.firstName + ' ' + item.lastName}
                                    </Text>
                                    <Text style={[text.h4, text.regular]}>
                                      {item.vehicleNo}
                                    </Text>
                                    <Text style={[text.h4]}>
                                      {new Date(Number(item.date * 1000)) + ' '}
                                    </Text>
                                  </View>
                                </View>
                                <View style={[appStyle.row, appStyle.aiFlexStart]}>
                                  {/* <Image
                                source={images.locationFrom}
                                style={image.locationFromIcon}
                              /> */}
                                  <Text
                                    style={[
                                      text.h4,
                                      text.black,
                                      text.medium,
                                      appStyle.ml10,
                                    ]}>
                                    {item.title}
                                  </Text>
                                </View>
                                {/* <View
                              style={[
                                appStyle.row,
                                appStyle.mt5,
                                appStyle.aiCenter,
                              ]}>
                              <Image
                                source={images.locationTo}
                                style={image.locationToIcon}
                              />
                              <Text
                                style={[text.h4, text.medium, appStyle.ml10]}>
                                Great North Road - Canal Road,
                              </Text>
                            </View> */}
                              </View>

                              {this.getStatusIcon(item.status)}
                            </View>

                            {/* <TouchableOpacity
                          style={style.routeMapIcon}
                          hitSlop={hitArea.hitArea}>
                          <Image
                            source={images.route}
                            style={[image.iconMd, appStyle.asFlexEnd]}
                          />
                        </TouchableOpacity> */}
                            <View style={[appStyle.wrapContainer, appStyle.jcFlexEnd]}>
                              <TouchableOpacity
                                onPress={() => this.onSelect(item.id)}
                                style={[button.btnRoute]}>
                                <Text style={[text.h4, text.white]}>Check Route</Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        );
                      })
                    ) : (
                        <View>
                          <View style={[style.margin40]} />
                          <Text
                            style={[
                              text.para,
                              text.black,
                              appStyle.mt10,
                              text.center,
                            ]}>
                            No tasks found.
                    </Text>
                        </View>
                      )}
                  </ScrollView>
                </View>
                {/* load more btn */}
                {/* <View
                  style={style.btnLoadMoreContainer}
                >
                  <TouchableOpacity>
                    <Text
                      style={[text.text16, text.medium, text.themeDefault, text.center]}
                    >Load more...</Text>
                  </TouchableOpacity>
                </View> */}
                {/* load more btn end */}
              </View>

              <View
                style={{
                  display: this.state.tabLiThird,
                  position: 'relative',
                  height: '100%',
                }}
              >
                <View
                  style={{
                    // height: screenHeight.height100 - 220
                    paddingBottom:40
                  }}
                >
              <ScrollView
                    contentContainerStyle={{
                      flexGrow: 1,
                      paddingTop: 20,
                    }}
                    showsVerticalScrollIndicator={false}
                  >
                {this.state.upcoming.length > 0 ? (
                  this.state.upcoming.map((item, index) => {
                    return (
                      <View style={[appStyle.shadow, style.routeContainer]}>
                        <View
                          style={[
                            appStyle.rowBtw,
                            appStyle.aiFlexStart,
                            appStyle.pv10,
                          ]}>
                          <View style={[appStyle.flex1, appStyle.pr15]}>
                            {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                            <View
                              style={[
                                appStyle.row,
                                appStyle.aiFlexStart,
                                appStyle.pb15,
                              ]}>
                              <Image
                                source={{ uri: item.driverImage }}
                                style={image.profileSm}
                              />
                              <View style={[appStyle.pl5]}>
                                <Text style={[text.h3, text.medium]}>
                                  {item.firstName + ' ' + item.lastName}
                                </Text>
                                <Text style={[text.h4, text.regular]}>
                                  {item.vehicleNo}
                                </Text>
                                <Text style={[text.h4]}>
                                  {new Date(Number(item.date * 1000)) + ' '}
                                </Text>
                              </View>
                            </View>
                            <View style={[appStyle.row, appStyle.aiFlexStart]}>
                              {/* <Image
                                source={images.locationFrom}
                                style={image.locationFromIcon}
                              /> */}
                              <Text
                                style={[
                                  text.h4,
                                  text.black,
                                  text.medium,
                                  appStyle.ml10,
                                ]}>
                                {item.title}
                              </Text>
                            </View>
                            {/* <View
                              style={[
                                appStyle.row,
                                appStyle.mt5,
                                appStyle.aiCenter,
                              ]}>
                              <Image
                                source={images.locationTo}
                                style={image.locationToIcon}
                              />
                              <Text
                                style={[text.h4, text.medium, appStyle.ml10]}>
                                Great North Road - Canal Road,
                              </Text>
                            </View> */}
                          </View>

                          {this.getStatusIcon(item.status)}
                        </View>

                        {/* <TouchableOpacity
                          style={style.routeMapIcon}
                          hitSlop={hitArea.hitArea}>
                          <Image
                            source={images.route}
                            style={[image.iconMd, appStyle.asFlexEnd]}
                          />
                        </TouchableOpacity> */}
                        <View style={[appStyle.wrapContainer, appStyle.jcFlexEnd]}>
                          <TouchableOpacity
                            onPress={() => this.onSelect(item.id)}
                            style={[button.btnRoute]}>
                            <Text style={[text.h4, text.white]}>Check Route</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    );
                  })
                ) : (
                    <View>
                      <View style={[style.margin40]} />
                      <Text
                        style={[
                          text.para,
                          text.black,
                          appStyle.mt10,
                          text.center,
                        ]}>
                        No tasks found.
                    </Text>
                    </View>
                  )}
              </ScrollView>
              </View>
              {/* load more btn */}
              {/* <View
                  style={style.btnLoadMoreContainer}
                >
                  <TouchableOpacity>
                    <Text
                      style={[text.text16, text.medium, text.themeDefault, text.center]}
                    >Load more...</Text>
                  </TouchableOpacity>
                </View> */}
                {/* load more btn end */}
              </View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </View>

        </View>
      </SafeAreaView>
    );
  }
}
