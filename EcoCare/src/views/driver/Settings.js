import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Alert,
  Linking,
} from 'react-native';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import ToggleSwitch from 'toggle-switch-react-native';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {updateUserStatus} from '../../network/Response';
import * as Helper from '../../components/Helper';
import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      loading: false,
      isOnDefaultToggleSwitch: true,

      user: {
        image: 'https://test-rexsolution.com/ecocare/assets/images/user.png',
        firstName: '',
        lastName: '',
        phone: '',
        email: '',
        vehicleNo: '',
      },
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Settings',
    headerStyle: {
      backgroundColor: colors.themeDefault,
      elevation: 0,
    },

    headerTitleStyle: {
      color: colors.white,
      textAlign: 'center',
      flex: 1,
    },
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getUserObject();
    });
    this.getUserObject();
  }

  async getUserObject() {
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    this.setState({user: user});
  }

  async onToggleSwitch(status) {
    var user = this.state.user;
    user['isOnline'] = status == true ? 1 : 0;
    var params = {
      status: status == true ? 1 : 0,
      userId: user.userId,
      sessionId: user.sessionId,
    };

    this.setState({loading: true});
    await updateUserStatus(params).then((response) => {
      if (response.status == 200) {
        saveUser('user', user);
        Helper.updateUserObject(user);
        this.setState({user: user, loading: false});
        Helper.showSnack(response.message);
      } else if (response.status == 400) {
        Helper.showSnack(response.message);
      } else {
        Helper.showSnack(response.message);
        this.props.navigation.navigate('Logout');
      }
    });
  }
  onToggle(isOn) {
    console.log('Changed to ' + isOn);
  }

  makeCall = (phoneNumber) => {
    console.log(phoneNumber);
    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${' + phoneNumber + '}';
    } else {
      phoneNumber = 'telprompt:${' + phoneNumber + '}';
    }

    Linking.openURL(phoneNumber);
  };

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainerSettings]}>
          <View style={style.settingsHeader}>
            <Image
              source={{uri: this.state.user.image}}
              style={image.profile}
            />
            <View style={[appStyle.pl15]}>
              <Text style={[text.h1, text.white, text.medium]}>
                {this.state.user.firstName + ' ' + this.state.user.lastName}
              </Text>
              <Text style={[text.h3, text.white, text.medium]}>
                {this.state.user.phone}
              </Text>
              <Text style={[text.h3, text.white, text.medium]}>
                {this.state.user.email}
              </Text>
              {this.state.user.userType == 3 && (
                <Text style={[text.h3, text.white, text.medium]}>
                  {this.state.user.vehicleNo}
                </Text>
              )}
            </View>
          </View>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              {this.state.user.userType == 3 && (
                <View style={[appStyle.rowBtw, appStyle.pb20]}>
                  <Text style={[text.h3, text.semibold, text.greyDark]}>
                    Update your online status
                  </Text>
                  <ToggleSwitch
                    // isOn={false}
                    onColor={colors.themeDefault}
                    offColor={colors.grey}
                    labelStyle={{color: 'black', fontWeight: '900'}}
                    size="medium"
                    // onToggle={isOn => console.log("changed to : ", isOn)}
                    isOn={this.state.user.isOnline == 1 ? true : false}
                    onToggle={(isOnDefaultToggleSwitch) => {
                      // this.setState({isOnDefaultToggleSwitch});
                      this.onToggleSwitch(isOnDefaultToggleSwitch);
                    }}
                  />
                </View>
              )}
              {/* <TouchableOpacity
                style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}
                onPress={() =>
                  this.props.navigation.navigate(
                    this.state.user.userType == 2
                      ? 'ManagerProfileSettings'
                      : 'DriverProfileSettings',
                  )
                }>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image
                    source={images.user}
                    style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Profile</Text>
                </View>

                <Image source={images.arrow} style={image.iconSettingsArrow} />
              </TouchableOpacity> */}

              <View style={style.borderBottom} />

              {/* <TouchableOpacity style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image source={images.bell} style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Notification</Text>
                </View>

                <Image source={images.arrow} style={image.iconSettingsArrow} />
              </TouchableOpacity>

              <View
                style={style.borderBottom} /> */}
              {this.state.user.userType == 3 && (
                <TouchableOpacity
                  style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}
                  onPress={() => this.props.navigation.navigate('History')}>
                  <View style={[appStyle.row, appStyle.aiCenter]}>
                    <Image
                      source={images.history}
                      style={[image.icon, appStyle.mr10]}
                    />
                    <Text style={[text.h2, text.black]}>Trip history</Text>
                  </View>

                  <Image
                    source={images.arrow}
                    style={image.iconSettingsArrow}
                  />
                </TouchableOpacity>
              )}

              {/* onPress={() =>
                  Linking.openURL('https://app.termly.io/document/terms-of-use-for-website/f404fc49-2367-4ec0-a3ad-010df35659d1')
                } */}
              <View style={style.borderBottom} />

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Logout')}
                style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image
                    source={images.logout}
                    style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Logout</Text>
                </View>
                <Image source={images.arrow} style={image.iconSettingsArrow} />
              </TouchableOpacity>
              <View style={style.borderBottom} />
              <TouchableOpacity
                onPress={(phoneNumber) => this.makeCall('+49 172 8499996')}
                style={[appStyle.rowBtw, appStyle.mb10, appStyle.mt20]}>
                <View style={[appStyle.row, appStyle.aiCenter]}>
                  <Image
                    source={images.logout}
                    style={[image.icon, appStyle.mr10]}
                  />
                  <Text style={[text.h2, text.black]}>Emergency Call</Text>
                </View>
                <Image
                  source={images.phone}
                  style={[
                    image.iconSettinsArrow,
                    {height: 40, width: 40, resizeMode: 'contain'},
                  ]}
                />
              </TouchableOpacity>
              <View style={[appStyle.rowCenter, appStyle.mt5]}>
              <Text style={[text.h3, text.light, text.medium]}>
                For more information,
              </Text>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    'http://88.85.125.77/ecocare/Privacy.html',
                  )
                }
                hitSlop={appStyle.hitSlopArea}
                style={[appStyle.mv5, appStyle.mh5]}>
                <Text style={[text.h3, text.themeDefault, text.medium]}>
                  Privacy Policy
                </Text>
              </TouchableOpacity>
            </View>
              <View></View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loading}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

async function saveUser(user, data) {
  try {
    await AsyncStorage.removeItem('user');
    await AsyncStorage.setItem(user, JSON.stringify(data));
  } catch (error) {}
}
