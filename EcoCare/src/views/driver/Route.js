import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  hitArea,
} from '../../config/Constant';
var firebase = require('firebase');
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import LeftComponent from '../../components/headerComponent/LeftComponent';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {getTaskRoutes} from '../../network/Response';
import * as Helper from '../../components/Helper';
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      routes: [],
      subTasks: [],
      loading: true,
      loader: false,
      listner: 111,
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Route',
    headerStyle: {
      backgroundColor: colors.white,
      // alignItems: 'center',
    },
    headerLeft: () => <LeftComponent />,

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
    headerTitleAlign: 'center',
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getTasks();
    });
    this.getTasks();
  }

  async eventLister() {
    firebase
      .database()
      .ref('event')
      .child('managerTasks')
      .on('value', (obj) => {
        if (obj['key'] != this.state.listner) {
          this.getTasks();
          this.setState({listner: obj['key']});
        }
      });
  }

  async getTasks() {
    this.setState({loader: true});
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var params = {};
    params['userId'] = user.userId;
    params['sessionId'] = user.sessionId;
    params['taskId'] = this.props.navigation.state.params.taskId;
    this.setState({user: user, taskId: params['taskId']});

    await getTaskRoutes(params).then((response) => {
      this.setState({loader: false});
      if (response.status == 200) {
        var routes = response.data.routes;
        var task = response.data.task;
        var subTasks = [];
        var collection = 0;
        var collectionDone = 0;
        var subTasks = routes;
        for (var i = 0; i < routes.length; i++) {
          collection = collection + Number(routes[i]['collectionCount']);
          if (routes[i]['status'] == 2) {
            collectionDone =
              collectionDone + Number(routes[i]['collectionCount']);
          }
          routes[i]['direction'] = 0;
        }
        task['collection'] = collection;
        task['collectionDone'] = collectionDone;

        this.setState({
          task: task,
          routes: routes,
          subTasks: response.data.directions,
          loading: false,
        });

        // if (user.userType == 3) {
          this.updateOnFirebase(task, routes);
        // }
        if (user.userType == 2) {
          this.eventLister();
        }
      } else if (response.status == 400) {
        Helper.showSnack(response.message);
      } else {
        Helper.showSnack(response.message);
        this.props.navigation.navigate('Logout');
      }
    });
  }

  async updateOnFirebase(task, routes) {
    task['routes'] = null;

    firebase
      .database()
      .ref('tasks' + '/' + task.id)
      .set(task)
      .then((res) => {
        for (var i = 0; i < routes.length; i++) {
          var dataToStore = routes[i];
          dataToStore['direction'] = '';
          firebase
            .database()
            .ref('tasks' + '/' + task.id + '/routes/' + routes[i]['id'] + '/')
            .set(dataToStore)
            .then((data) => {})
            .catch((error) => {
              console.log('error ', error);
            });
        }
      })
      .catch((error) => {
        console.log('error ', error);
      });

    // var p1 = {
    //   key: 121212,
    // };
    // var p2 = {
    //   key: 121212,
    // };
    // var p3 = {
    //   key: 121212,
    // };
    // firebase
    //   .database()
    //   .ref('event' + '/managerTasks/')
    //   .set(p1)
    //   .catch((error) => {
    //     console.log('error ', error);
    //   });

    // firebase
    //   .database()
    //   .ref('event' + '/Routes/')
    //   .set(p2)
    //   .catch((error) => {
    //     console.log('error ', error);
    //   });

    // firebase
    //   .database()
    //   .ref('event' + '/routeUpdate/')
    //   .set(p3)
    //   .catch((error) => {
    //     console.log('error ', error);
    //   });
  }
  // async onSelect(id) {
  //   this.props.navigation.navigate('DriverTaskRoutes', {
  //     taskId: id,
  //   });
  // }

  getStatusIcon(status) {
    if (status == 0) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.upcoming} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>Upcoming</Text>
        </View>
      );
    } else if (status == 1) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.current} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>On the way</Text>
        </View>
      );
    } else if (status == 2) {
      return (
        <View style={[appStyle.row, appStyle.aiCenter]}>
          <Image source={images.complete} style={[image.iconSm]} />
          <Text style={[text.h4, appStyle.ml5]}>Completed</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  async onSelect(route, i) {
    var subTasks = this.state.subTasks;
    this.props.navigation.navigate(
      this.state.user.userType == 3 ? 'DriverRouteMap' : 'ManagerRouteMap',
      {
        taskId: this.state.task.id,
        routeId: route.id,
        direction: subTasks[i]['content'],
        task: this.state.task,
      },
    );
  }

  render() {
    if (this.state.loading) {
      return null;
    }
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>{this.state.task.title}</Text>

                <Text style={[text.h4, text.medium]}>
                  {'Collections : ' +
                    this.state.task.collectionDone +
                    '/' +
                    this.state.task.collection}
                </Text>
              </View>

              <View>
                {this.state.routes.map((item, index) => {
                  return (
                    <View style={[appStyle.shadow, style.routeContainer]}>
                      <View
                        style={[
                          appStyle.rowBtw,
                          appStyle.aiFlexStart,
                          appStyle.pv10,
                        ]}>
                        <View style={[appStyle.flex1, appStyle.pr20]}>
                          {/* <Image source={images.locationLine}
                                                style={{ position: 'absolute', left: 7 }}
                                            /> */}
                          <View style={[appStyle.row, appStyle.aiFlexStart]}>
                            <Image
                              source={images.locationFrom}
                              style={image.locationFromIcon}
                            />
                            <Text
                              style={[
                                text.h4,
                                text.black,
                                text.medium,
                                appStyle.ml10,
                              ]}>
                              {item.startAddress}
                            </Text>
                          </View>
                          <View
                            style={[
                              appStyle.row,
                              appStyle.mt5,
                              appStyle.aiCenter,
                            ]}>
                            <Image
                              source={images.locationTo}
                              style={image.locationToIcon}
                            />

                            <Text style={[text.h4, text.medium, appStyle.ml10]}>
                              {item.endAddress}
                            </Text>
                          </View>
                          <Text
                            style={[text.h5, appStyle.ml5, {paddingLeft: 20}]}>
                            Distance: {item.distanceText}
                          </Text>
                          <Text
                            style={[text.h5, appStyle.ml5, {paddingLeft: 20}]}>
                            Duration: {item.durationText}
                          </Text>
                          <Text
                            style={[text.h5, appStyle.ml5, {paddingLeft: 20}]}>
                            Collections: {item.collectionCount}
                          </Text>
                        </View>
                        {this.getStatusIcon(item.status)}
                      </View>
                      {/* {this.state.user.userType == 3 && (
                        <View>
                          <TouchableOpacity
                            onPress={() => this.onSelect(item, index)}
                            style={style.routeMapIcon}
                            hitSlop={hitArea.hitArea}>
                            <Image
                              source={images.route}
                              style={[image.iconMd, appStyle.asFlexEnd]}
                            />
                          </TouchableOpacity>
                        </View>
                      )} */}

                      {/* {this.state.user.userType == 2 &&
                        (item.status == 1 || item.status == 2) && ( */}
                          <View>
                            <TouchableOpacity
                              onPress={() => this.onSelect(item, index)}
                              style={style.routeMapIcon}
                              hitSlop={hitArea.hitArea}>
                              <Image
                                source={images.route}
                                style={[image.iconMd, appStyle.asFlexEnd]}
                              />
                            </TouchableOpacity>
                          </View>
                        {/* )} */}
                    </View>
                  );
                })}
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
