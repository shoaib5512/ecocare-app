import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import { colors, screenHeight, screenWidth, images } from '../../config/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import LeftComponent from '../../components/headerComponent/LeftComponent';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import { getDriverTask } from '../../network/Response';
import * as Helper from '../../components/Helper';
export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      tasks: [],
      loader: false,
    };
  }
  static navigationOptions = {
    title: 'Trip History',
    headerStyle: {
      backgroundColor: colors.white,
      // alignItems: 'center',
    },
    headerLeft: () => <LeftComponent />,

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
    headerTitleAlign: 'center',
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getTasks();
    });
    this.getTasks();
  }

  async getTasks() {
    this.setState({ loader: true });
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var params = {};
    params['userId'] = user.userId;
    params['sessionId'] = user.sessionId;
    params['status'] = 2;
    params['offset'] = this.state.offset;
    this.setState({ user: user });
    console.warn(params);
    let response = await getDriverTask(params);
    this.setState({ loader: false });
    if (response.status == 200) {
      this.setState({
        tasks: response.data.tasks,
        offset: this.state.offset + 1,
      });
    } else if (response.status == 400) {
      Helper.showSnack(response.message);
    } else {
      Helper.showSnack(response.message);
      this.props.navigation.navigate('Logout');
    }
  }

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <View style={{
            height: screenHeight.height100 - 110,
            // flex:1,
            // backgroundColor:'orange'
          }}>

            <KeyboardAwareScrollView
              contentContainerStyle={[appStyle.scrollContainerCenter]}
              showsVerticalScrollIndicator={false}

            >
              <View style={[appStyle.wrapper]}>
                <View>
                  {this.state.tasks.length > 0 ? (
                    this.state.tasks.map((item, index) => {
                      return (
                        <View style={[appStyle.shadow, style.routeContainer]}>
                          <Text style={[text.h2, text.black]}>
                            {item.title}
                          </Text>
                          <Text style={[text.h4]}> {new Date(Number(item.date * 1000)) + ' '}</Text>

                          <View style={[appStyle.rowBtw, appStyle.mt10]}>
                            <Text style={[text.h4, text.black, text.medium]}>
                              Total route : {item.routes}
                            </Text>
                          </View>
                        </View>
                      );
                    })
                  ) : (
                      <View>
                        <View style={[style.margin40]} />
                        <Text
                          style={[
                            text.para,
                            text.black,
                            appStyle.mt10,
                            text.center,
                          ]}>
                          No tasks found.
                    </Text>
                      </View>
                    )}
                </View>
              </View>
              <Dialog
                dialogStyle={appStyle.dialogLoader}
                visible={this.state.loader}
                dialogAnimation={new ScaleAnimation()}
                overlayOpacity={0}>
                <DialogContent>
                  <View style={appStyle.dialogActivityContainer}>
                    <ActivityIndicator size="large" color={colors.red} />
                  </View>
                </DialogContent>
              </Dialog>
            </KeyboardAwareScrollView>
          </View>

          {/* load more btn */}
          {/* <View
                  style={[style.btnLoadMoreContainer,{
                    height:70,
                    // backgroundColor:'#fff'
                  }]}
                >
                  <TouchableOpacity>
                    <Text
                      style={[text.text16, text.medium, text.themeDefault, text.center]}
                    >Load more...</Text>
                  </TouchableOpacity>
                </View> */}
          {/* load more btn end */}
        </View>
      </SafeAreaView>
    );
  }
}
