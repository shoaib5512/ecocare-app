import React, {Component, createRef} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  BackHandler,
} from 'react-native';
import {
  colors,
  screenHeight,
  screenWidth,
  images,
  hitArea,
} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {RouteButton} from '../../components/headerComponent/RightComponent';
import LeftComponent from '../../components/headerComponent/LeftComponent';
import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
// import SignatureCapture from 'react-native-signature-capture';
// import SignaturePad from 'react-native-signature-pad';
import SignatureCapture from 'react-native-signature-capture-spatacus';

import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
  ButtonDefaultwhite,
  ButtonDefaultSlim,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {updateRouteStatus} from '../../network/Response';
import {
  getLocationPermission,
  getReadPermission,
  getWritePermission,
} from '../../network/Permission';
import * as Helper from '../../components/Helper';
import ActionSheet from 'react-native-actions-sheet';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
var firebase = require('firebase');
import Geolocation from '@react-native-community/geolocation';
import {app, apps} from 'firebase';
// import PolylineDirection from '@react-native-maps/polyline-direction';
const origin = {latitude: 31.5203696, longitude: 74.35874729999999};
const destination = {latitude: 33.6844202, longitude: 73.04788479999999};
const GOOGLE_MAPS_APIKEY = 'AIzaSyDmyqHkq8-58a37slrRpQVmdC0ZccB0JV4';
// AIzaSyDojgtv3wqnTdU10RwP2e9uqxW5E7BQxNM
const actionSheetRef = createRef();
const actionSheetRefOnEndBtn = createRef();
const ref = createRef();
var signatureImage = '';
export default class RouteMap extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      value: 'name',
      coords: [],
      loading: true,
      loader: false,
      taskId: 0,
      origin: {latitude: 31.5203696, longitude: 73.0478847999999},
      activeRoute: {
        startName: '',
        endName: '',
        status: 0,
        startLatitude: 31.5203696,
        startLongitude: 73.0478847999999,
      },
      driverLocation: {latitude: '', longitude: ''},
      activeRouteNum: 0,
      isMapLoaded: false,
      signatureShow: true,
      signatureImage: '',
      receiverName: '',
    };
  }

  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    clearInterval(this.interval);
  }

  handleBackButtonClick() {
    this.setState({taskId: 0});
    this.props.navigation.goBack(null);
    return true;
  }

  async componentDidMount() {
    // this.getDirections(
    //   '31.5203696, 73.0478847999999',
    //   '32.5203696,74.0478847999999',
    // );
    // actionSheetRef.current?.setModalVisible(true);

    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var taskId = this.props.navigation.state.params.taskId;
    var routeId = this.props.navigation.state.params.routeId;
    var task = this.props.navigation.state.params.task;
    var direction = this.props.navigation.state.params.direction;
    this.setState({
      user: user,
      taskId: taskId,
      routeId: routeId,
      task: task,
      direction: direction,
    });
    console.log(task);
    if (this.state.user.userType == 3) {
      this.updateUserLocation();
    }
    this.getGetRouteDetail();
    this.interval = setInterval(
      () =>
        Number(this.state.user.userType) == 3
          ? this.updateUserLocation()
          : console.log('calling manager'),
      5000,
    );

    if (Platform.OS == 'android' && this.state.user.userType == 3) {
      let permRead = await getWritePermission();
      let permWrite = await getReadPermission();
    }
  }

  async getGetRouteDetail() {
    var taskId = this.state.taskId;
    var routeId = this.state.routeId;
    firebase
      .database()
      .ref('tasks')
      .child(taskId)
      .on('value', (taskObj) => {
        if (this.state.taskId != 0) {
          if (taskObj.val() != null) {
            if (
              taskObj.val().routes != null ||
              taskObj.val().routes != undefined
            ) {
              var routes = [];
              var count = 1;
              Object.values(taskObj.val().routes).forEach((obj) => {
                // if (obj['isActive'] == 1) {
                if (this.state.routeId == obj.id) {
                  if (obj.status == 1) {
                    //   alert(this.state.task.assignTo);
                    firebase
                      .database()
                      .ref('location')
                      .child(this.state.task.assignTo)
                      .on('value', (childObj) => {
                        var driverLoc = {
                          latitude: childObj.val().latitude,
                          longitude: childObj.val().longitude,
                        };
                        this.setState({driverLocation: driverLoc});
                        var dis = Helper.getGeoDistance(
                          childObj.val().latitude,
                          childObj.val().longitude,
                          obj.endLatitude,
                          obj.endLongitude,
                        );
                        // console.log('*******************', dis);

                        // if (dis > 0 && dis <= 1) {
                        //   this.onPressButton(2);
                        // }
                      });
                  }

                  var org = {
                    latitude: obj.startLatitude,
                    longitude: obj.startLongitude,
                  };
                  this.setState({
                    activeRoute: obj,
                    routeId: obj.id,
                    loading: false,
                    activeRouteNum: count,

                    // origin:org,
                  });
                  var startLocation = [obj.startLatitude, obj.startLongitude];
                  var endLocation = [obj.endLatitude, obj.endLongitude];
                  this.getDirections(startLocation, endLocation);
                }
                routes.push(obj);
                count++;
              });
              this.setState({routes: routes});
            }
          }
        }
      });
  }

  // Geolocation.watchPosition(
  //   (position) => {
  //     console.warn(' + + + + + +  + + + + ++  + + + + + + + + + + + + + + ++ ',position);

  async updateUserLocation() {
    var user = this.state.user;
    let permission = await getLocationPermission();
    if (permission) {
      Geolocation.getCurrentPosition(
        (position) => {
          //   this.setState({coords: position.coords});

          var coords = position.coords;
          if (coords.latitude != undefined || coords != '') {
            //permission ok

            var userLocation = {
              userId: user.userId,
              latitude: coords.latitude,
              longitude: coords.longitude,
            };

            this.setState({driverLocation: userLocation});

            console.warn(
              ' + + + + + +  + + + + ++  + + + + + + + + + + + + + + ++ ',
              userLocation,
            );
            Helper.updateUserLocation(userLocation);
          } else {
            this.setState({loader: false});
            Helper.showSnack('You cannot use current location feature.');
            // this.props.navigation.navigate(nav);
          }
        },
        (error) => {
          // Helper.showSnack('Something went wrong while getting your location.');
          this.setState({loading: false, loader: false});
          //   this.props.navigation.navigate(nav);
        },

        {enableHighAccuracy: false, timeout: 10000, maximumAge: 10000},
      );
    } else {
      this.setState({loading: false, loader: false});
      // Helper.showSnack('Something went wrong while getting your location.');
      //   this.props.navigation.navigate(nav);
    }
  }
  async getDirections(startLoc, destinationLoc) {
    var direction = this.state.direction;
    let points = Polyline.decode(direction.routes[0].overview_polyline.points);
    let coords = points.map((point, index) => {
      return {
        latitude: point[0],
        longitude: point[1],
      };
    });
    this.setState({coords: coords, isMapLoaded: true});
    return false;
    // try {
    //   let resp = await fetch(
    //     `https://maps.googleapis.com/maps/api/directions/json?mode=driving&key=${GOOGLE_MAPS_APIKEY}&origin=${startLoc}&destination=${destinationLoc}`,
    //   );
    //   let respJson = await resp.json();
    //   console.warn(respJson);
    //   let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
    //   console.warn('>>>>>', points);
    //   return false;
    //   let coords = points.map((point, index) => {
    //     return {
    //       latitude: point[0],
    //       longitude: point[1],
    //     };
    //   });
    //   this.setState({coords: coords, isMapLoaded: true});
    //   return coords;
    // } catch (error) {
    //   Helper.showSnack(error);
    //   return error;
    // }
  }

  onPressRouteDetail() {
    actionSheetRef.current?.setModalVisible(true);
  }

  onPressRouteActionModal() {


    actionSheetRef.current?.setModalVisible(false);
    setTimeout(
      function () {
        actionSheetRefOnEndBtn.current?.setModalVisible(true);
      }.bind(this),
      500,
    );
    // actionSheetRef.current?.setModalVisible(false);
    // setTimeout(
    //   function () {
    //     actionSheetRefOnEndBtn.current?.setModalVisible(false);
    //     this.props.navigation.navigate('DriverScannner', {
    //       onSelect: this.onSelect,
    //     });
    //   }.bind(this),
    //   500,
    // );

  }

  async onPressButton1(status) {
    this.saveSign();
    setTimeout(
      function () {
        this.onPressButton(status);
      }.bind(this),
      500,
    );
  }

  openScanner() {

    actionSheetRef.current?.setModalVisible(false);
    setTimeout(
      function () {
        this.props.navigation.navigate('DriverScannner', {
          onSelect: this.onSelect,
        });
      }.bind(this),
      500,
    );

  }

  onSelect = () => {
    this.onPressButton(1);
  };

  async onPressButton(status) {
    console.warn('> > > > > > > > > > > > > > > > > > ', status);

    // return false;
    // actionSheetRef.current?.setModalVisible(false);

    // return false;
    var route = this.state.activeRoute;
    var routes = this.state.routes;
    var user = this.state.user;
    if (user.isOnline == 0) {
      Helper.showSnack('You are not online, Please update your online status.');
      return false;
    }

    var params = {
      routeId: route.id,
      status: status,
      userId: user.userId,
      sessionId: user.sessionId,
    };

    if (status == 2) {
      if (signatureImage == '') {
        Helper.showSnack('Please get the receiver signature.');
        return false;
      }
      this._onSaveEvent;
      if (this.state.receiverName == '') {
        Helper.showSnack('Please Enter the receiver name.');
        return false;
      }
      params['receiverName'] = this.state.receiverName;
      params['image'] = 'file://' + signatureImage;
      params['imageimage '] = 'file://' + signatureImage;
    } else {
      params['image'] = '';
    }
    actionSheetRefOnEndBtn.current?.setModalVisible(false);
    console.warn('> > > > > > > > > > > > > > > > > > ', params['image ']);
    // return false;
    var img = params['image'];
    this.setState({loader: true});
    await updateRouteStatus(params).then((response) => {
      this.setState({loader: false});
      if (response.status == 200) {
        //   this.setState({user: user, loading: false});
        Helper.showSnack(
          status == 1
            ? 'Now you are on the way.'
            : 'task completed successfully',
        );
        // Helper.showSnack(response.message);
        actionSheetRef.current?.setModalVisible(false);
        if (
          this.state.activeRouteNum != this.state.routes.length &&
          this.state.routes.length != 1
        ) {
          route['isActive'] = status == 1 ? 1 : 0;
          route['status'] = status;
        } else {
          route['isActive'] = 1;
          route['status'] = status;
        }
        if (this.state.user.userType == 3 && status == 2) {
          this.props.navigation.goBack();
        }

        var ro = route['isActive'];
        firebase
          .database()
          .ref('tasks' + '/' + route.taskId + '/routes/' + route['id'] + '/')
          .set(route)
          .then((data) => {
            // if (ro == 0) {
            //   var routes = this.state.routes;
            //   var newRoute = routes[this.state.activeRouteNum + 1];
            //   newRoute['isActive'] = 1;
            //   firebase
            //     .database()
            //     .ref(
            //       'tasks' +
            //         '/' +
            //         newRoute.taskId +
            //         '/routes/' +
            //         newRoute['id'] +
            //         '/',
            //     )
            //     .set(newRoute)
            //     .then((data) => {})
            //     .catch((error) => {
            //       console.log('error ', error);
            //     });
            //   this.setState({
            //     routeId: newRoute['id'],
            //     activeRoute: newRoute,
            //     activeRouteNum: this.state.activeRouteNum + 1,
            //   });
            //   this.getGetRouteDetail();
            // }
          })
          .catch((error) => {
            console.log('error ', error);
          });
      } else if (response.status == 400) {
        Helper.showSnack(response.message);
      } else {
        Helper.showSnack(response.message);
        // this.props.navigation.navigate('Logout');
      }
    });
  }
  static navigationOptions = {
    title: 'Route',
    headerStyle: {
      backgroundColor: colors.white,
      // alignItems: 'center',
    },
    headerLeft: () => <LeftComponent />,
    headerRight: () => (
      <RouteButton
        onPress={() => {
          actionSheetRef.current?.setModalVisible(true);
        }}
      />
    ),

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
    headerTitleAlign: 'center',
  };

  // signature
  _signaturePadError = (error) => {
    console.error(error);
  };

  _signaturePadChange = ({base64DataUrl}) => {
    console.log(
      'Got new signature+ + +    ++++ ++ ++ + + +  ++: ' + base64DataUrl,
    );
    this.setState({signatureImage: base64DataUrl});
  };
  async onPressClear() {
    this.setState({signatureShow: false, signatureImage: ''});
    this.setState({signatureShow: true, signatureImage: ''});
  }
  // signaure end

  saveSign() {
    this.refs['sign'].saveImage();
    this._onSaveEvent;
  }

  resetSign() {
    this.refs['sign'].resetImage();
    signatureImage = '';
  }

  _onSaveEvent(result) {
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    console.log(result);
    signatureImage = result.pathName;
    console.warn('---  -- - - - - - - - - -', signatureImage);
  }
  _onDragEvent(rer) {
    // This callback will be called when the user enters signature
    console.log(rer);
  }

  render() {
    let actionSheet;
    if (this.state.loading) {
      return null;
    }

    return (
      <SafeAreaView style={[appStyle.flex1, appStyle.bgWhite]}>
        <View
          style={[appStyle.mainContainer, {paddingTop: 5, paddingBottom: 0}]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper, {paddingTop: 0}]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>
                  {this.state.task.title} (
                  {this.state.activeRoute.collectionCount})
                </Text>
                <Text style={[text.h5]}>
                  {this.state.activeRoute.startName +
                    ' - ' +
                    this.state.activeRoute.endName}
                </Text>
                {this.state.activeRoute.isActive == 1 &&
                  this.state.user.userType == 3 && (
                    <View>
                      {this.state.activeRoute.status == 0 && (
                        <ButtonDefaultSlim
                          onPress={() => this.onPressRouteDetail()}
                          maxWidth={'60%'}>
                          Start Route
                        </ButtonDefaultSlim>
                      )}
                      {this.state.activeRoute.status == 1 && (
                        <ButtonDefaultSlim
                          onPress={() => this.onPressRouteDetail()}
                          maxWidth={'60%'}>
                          Finish Route
                        </ButtonDefaultSlim>
                      )}
                    </View>
                  )}
              </View>
              {this.state.isMapLoaded && (
                <MapView
                  style={style.mapView}
                  initialRegion={{
                    latitude: Number(this.state.activeRoute.startLatitude),
                    longitude: Number(this.state.activeRoute.startLongitude),
                    latitudeDelta: 3.5,
                    longitudeDelta: 3,
                  }}>
                  <MapView.Polyline
                    coordinates={this.state.coords}
                    strokeWidth={5}
                    strokeColor="red"
                  />
                  {this.state.driverLocation.latitude != '' &&
                    this.state.activeRoute.status == 1 && (
                      <MapView.Marker
                        coordinate={{
                          latitude: Number(this.state.driverLocation.latitude),
                          longitude: Number(
                            this.state.driverLocation.longitude,
                          ),
                        }}>
                        <Image
                          source={images.marker}
                          style={{width: 70, height: 70}}
                        />
                      </MapView.Marker>
                    )}

                  <MapView.Marker
                    coordinate={{
                      latitude: Number(this.state.activeRoute.startLatitude),
                      longitude: Number(this.state.activeRoute.startLongitude),
                    }}>
                    <Image
                      source={images.start}
                      style={{width: 40, height: 40}}
                    />
                  </MapView.Marker>

                  <MapView.Marker
                    coordinate={{
                      latitude: Number(this.state.activeRoute.endLatitude),
                      longitude: Number(this.state.activeRoute.endLongitude),
                    }}>
                    <Image
                      source={images.end}
                      style={{width: 40, height: 40}}
                    />
                  </MapView.Marker>
                </MapView>
              )}

              {/* <MapView
                style={style.mapView}
                region={{
                  latitude: 31.5203696,
                  longitude: 73.04788479999999,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}>
                <PolylineDirection
                  origin={origin}
                  destination={destination}
                  apiKey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={10}
                  strokeColor="#ff0000"
                />
              </MapView> */}

              {/* <MapView
                mode={'driving'}
                style={style.mapView}
                region={{
                  latitude: 31.5203696,
                  longitude: 73.04788479999999,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}>
                <Polyline
                  lineJoin={'round'}
                  coordinates={[
                    {latitude: 31.5203696, longitude: 74.35874729999999},

                    {latitude: 31.5203696, longitude: 73.04788479999999},
                  ]}
                  strokeColor="#f00"
                  strokeColors={['#7F0000', '#B24112']}
                  strokeWidth={6}
                />
              </MapView> */}
              <View>
                <ActionSheet
                  ref={actionSheetRef}
                  containerStyle={style.actionSheet}
                  style={style.actionSheet}
                  animated={true}
                  headerAlwaysVisible={true}
                  bounceOnOpen={true}
                  overlayColor={colors.black}
                  defaultOverlayOpacity={0.45}
                  elevation={10}
                  footerHeight={2}
                  statusBarTranslucent={false}
                  indicatorColor={'#C6C8CB'}
                  // closeOnPressBack={false}
                >
                  <View style={[style.actionSheetContainer, appStyle.p20]}>
                    <View
                      style={[
                        style.routeContainer,
                        appStyle.p0,
                        {borderColor: '#fff'},
                      ]}>
                      <View
                        style={[
                          appStyle.rowBtw,
                          appStyle.aiFlexStart,
                          appStyle.pv10,
                        ]}>
                        <View style={[appStyle.flex1, appStyle.pr20]}>
                          <Image
                            source={images.locationLine}
                            style={{position: 'absolute', left: 7}}
                          />
                          <View style={[appStyle.row, appStyle.aiFlexStart]}>
                            <Image
                              source={images.locationFrom}
                              style={image.locationFromIcon}
                            />
                            <Text
                              style={[
                                text.h4,
                                text.black,
                                text.medium,
                                appStyle.ml10,
                              ]}>
                              {this.state.activeRoute.startName}
                            </Text>
                          </View>
                          <View
                            style={[
                              appStyle.row,
                              appStyle.mt5,
                              appStyle.aiCenter,
                            ]}>
                            <Image
                              source={images.locationTo}
                              style={image.locationToIcon}
                            />
                            <Text style={[text.h4, text.medium, appStyle.ml10]}>
                              {this.state.activeRoute.endName}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>

                    <ScrollView>
                      <View>
                        {this.state.activeRoute.status == 1 && (
                          <View style={[appStyle.row, appStyle.pb15]}>
                            <Image source={images.current} style={image.icon} />
                            <Text style={[text.h3, appStyle.ml10]}>
                              On the way
                            </Text>
                          </View>
                        )}
                        {this.state.activeRoute.status == 0 && (
                          <View style={[appStyle.row, appStyle.pb15]}>
                            <Image
                              source={images.upcoming}
                              style={image.icon}
                            />
                            <Text style={[text.h3, appStyle.ml10]}>
                              Upcoming
                            </Text>
                          </View>
                        )}
                        {this.state.activeRoute.status == 2 && (
                          <View style={[appStyle.row, appStyle.pb15]}>
                            <Image
                              source={images.complete}
                              style={image.icon}
                            />
                            <Text style={[text.h3, appStyle.ml10]}>
                              Completed
                            </Text>
                          </View>
                        )}
                      </View>
                    </ScrollView>

                    <View>
                      <Text style={[text.h3, appStyle.mr20]}>
                        Distance:{' '}
                        <Text style={[text.h3, text.greyDark, appStyle.pl5]}>
                          {this.state.activeRoute.distanceText}
                        </Text>
                      </Text>
                      <Text style={[text.h3, appStyle.mr20]}>
                        Duration:{' '}
                        <Text style={[text.h3, text.greyDark, appStyle.pl5]}>
                          {this.state.activeRoute.durationText}
                        </Text>
                      </Text>
                      <Text style={[text.h3, appStyle.mr20]}>
                        Collections:{' '}
                        <Text style={[text.h3, text.greyDark, appStyle.pl5]}>
                          {this.state.activeRoute.collectionCount}
                        </Text>
                      </Text>
                    </View>
                    <View>
                      {this.state.activeRoute.isActive == 1 &&
                        this.state.user.userType == 3 && (
                          <View>
                            {this.state.activeRoute.status == 0 && (
                              <ButtonDefault
                                // onPress={() => this.onPressRouteActionModal()}
                                onPress={() => this.openScanner()}
                                >
                                Start
                              </ButtonDefault>
                            )}
                            {this.state.activeRoute.status == 1 && (
                              <View>
                                <View style={[appStyle.row, appStyle.aiCenter]}>
                                  <Image
                                    source={images.complete}
                                    style={image.locationToIcon}
                                  />
                                  <Text
                                    style={[
                                      text.h3,
                                      text.greyDark,
                                      appStyle.pl5,
                                    ]}>
                                    You have confirmed the collection
                                    information{' '}
                                  </Text>
                                </View>
                                <ButtonDefault
                                  onPress={() =>
                                    this.onPressRouteActionModal()
                                  }>
                                  End
                                </ButtonDefault>
                              </View>
                            )}
                          </View>
                        )}
                    </View>
                  </View>
                </ActionSheet>
                <ActionSheet
                  ref={actionSheetRefOnEndBtn}
                  containerStyle={style.actionSheet}
                  style={style.actionSheet}
                  animated={true}
                  headerAlwaysVisible={true}
                  bounceOnOpen={true}
                  overlayColor={colors.black}
                  defaultOverlayOpacity={0.45}
                  elevation={10}
                  footerHeight={2}
                  statusBarTranslucent={false}
                  indicatorColor={'#C6C8CB'}
                  // closeOnPressBack={false}
                >
                  <View
                    style={[
                      style.actionSheetContainer,
                      appStyle.p20,
                      {
                        height:
                          this.state.activeRoute.status == 1
                            ? screenHeight.height100
                            : 200,
                      },
                    ]}>
                    <ScrollView>
                      {this.state.activeRoute.status == 1 && (
                        <View>
                          <Text style={[text.h2, appStyle.pt10]}>
                            Enter receiver detail
                          </Text>
                          <Text style={[text.h3, text.greyDark, appStyle.pt5]}>
                            Receiver Signature:
                          </Text>
                          <View
                            style={{
                              flex: 1,
                              borderWidth: 1,
                              borderColor: '#dadce0',
                              borderRadius: 8,
                              overflow: 'hidden',
                              height: 250,
                            }}>
                            <SignatureCapture
                              style={[{flex: 1}, styles.signature]}
                              ref="sign"
                              onSaveEvent={this._onSaveEvent}
                              onDragEvent={this._onDragEvent}
                              saveImageFileInExtStorage={true}
                              showNativeButtons={true}
                              showTitleLabel={false}
                              viewMode={'portrait'}
                            />
                          </View>
                          <View>
                            <Text
                              style={[text.h3, text.greyDark, appStyle.pt20]}>
                              Receiver Name:
                            </Text>
                            <View style={[input.formInput]}>
                              <TextInput
                                style={input.input}
                                placeholder={'Enter Name'}
                                placeholderTextColor={colors.placeholder}
                                onChangeText={(receiverName) =>
                                  this.setState({receiverName: receiverName})
                                }
                                value={this.state.receiverName}
                              />
                            </View>
                          </View>
                        </View>
                      )}
                      {this.state.activeRoute.status == 0 && (
                        <Text style={[text.h2, text.greyDark, appStyle.pt5]}>
                          Are you sure you want to start this task.
                        </Text>
                      )}
                      {this.state.activeRoute.status == 1 && (
                        <Text style={[text.h2, text.greyDark, appStyle.pt5]}>
                          Are you sure you want to complete this task.
                        </Text>
                      )}

                      <View>
                        <View style={[appStyle.rowBtw, {padding: 5}]}>
                          {this.state.activeRoute.status == 0 && (
                            <ButtonDefault
                              onPress={() => this.openScanner()}
                              maxWidth={'40%'}>
                              Start
                            </ButtonDefault>
                          )}
                          {this.state.activeRoute.status == 1 && (
                            <ButtonDefault
                              onPress={() => this.onPressButton1(2)}
                              maxWidth={'40%'}>
                              End
                            </ButtonDefault>
                          )}
                          <ButtonDefaultwhite
                            onPress={() =>
                              actionSheetRefOnEndBtn.current?.setModalVisible(
                                false,
                              )
                            }
                            maxWidth={'40%'}>
                            Cancel
                          </ButtonDefaultwhite>
                        </View>
                      </View>
                    </ScrollView>
                  </View>
                </ActionSheet>
              </View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  signature: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 1,
  },
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#eeeeee',
    margin: 10,
  },
});
