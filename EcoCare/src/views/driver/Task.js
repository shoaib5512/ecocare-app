import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {getDriverTask} from '../../network/Response';
import * as Helper from '../../components/Helper';

export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      tasks: [],
      loader: false,
    };
  }

  static navigationOptions = {
    headerTintColor: colors.themeDefault,
    title: 'Tasks',
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTitleAlign: 'center',

    headerTitleStyle: {
      color: colors.themeDefault,
      textAlign: 'center',
    },
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getTasks();
    });
    this.getTasks();
  }

  async getTasks() {
    this.setState({loader: true});
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    var params = {};
    params['userId'] = user.userId;
    params['sessionId'] = user.sessionId;
    params['offset'] = this.state.offset;
    this.setState({user: user});
    console.warn(params);
    let response = await getDriverTask(params);
    this.setState({loader: false});
    if (response.status == 200) {
      this.setState({
        tasks: response.data.tasks,
        offset: this.state.offset + 1,
      });
    } else if (response.status == 400) {
      Helper.showSnack(response.message);
    } else {
      Helper.showSnack(response.message);
      this.props.navigation.navigate('Logout');
    }
  }

  async onSelect(id) {
    this.props.navigation.navigate('DriverTaskRoutes', {
      taskId: id,
    });
  }

  render() {
    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainer]}>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>Your Tasks</Text>
              </View>

              <View>
                {this.state.tasks.length > 0 ? (
                  this.state.tasks.map((item, index) => {
                    return (
                      <View style={[appStyle.shadow, style.taskContainer]}>
                        <Text style={[text.h2]}>{item.title}</Text>
                        <Text style={[text.h4]}>
                          {new Date(Number(item.date * 1000)) + ' '}
                        </Text>
                        <Text style={[text.para, text.black, appStyle.mt10]}>
                          {item.description}
                        </Text>
                        <View style={[appStyle.rowBtw, appStyle.mt10]}>
                          <Text style={[text.h4, text.black, text.medium]}>
                            Total route : {item.routes}
                          </Text>
                          <TouchableOpacity
                            onPress={() => this.onSelect(item.id)}
                            style={[button.btnRoute]}>
                            <Text style={[text.h4, text.white]}>
                              Check Route
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    );
                  })
                ) : (
                  <View>
                    <View style={[style.margin40]} />
                    <Text
                      style={[
                        text.para,
                        text.black,
                        appStyle.mt10,
                        text.center,
                      ]}>
                      No tasks found.
                    </Text>
                  </View>
                )}
              </View>
            </View>
            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loader}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
