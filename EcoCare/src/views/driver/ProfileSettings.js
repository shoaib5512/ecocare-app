import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Button,
  StatusBar,
  TextInput,
  TouchableOpacity,
  CheckBox,
  Image,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  Alert,
} from 'react-native';
import {colors, screenHeight, screenWidth, images} from '../../config/Constant';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import ImagePicker from 'react-native-image-picker';

import appStyle from '../../assets/styles/appStyle';
import style from '../../assets/styles/style';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import {updateProfile} from '../../network/Response';
import * as Helper from '../../components/Helper';
import {
  ButtonDefault,
  ButtonOutline,
  Input,
  ButtonText,
} from '../../components/Action';
import LeftComponent from '../../components/headerComponent/LeftComponent';
import LeftComponentWhite from '../../components/headerComponent/LeftComponentWhite';

export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: 'name',
      photo: 'https://test-rexsolution.com/ecocare/assets/images/user.png',
      user: {
        image: 'https://test-rexsolution.com/ecocare/assets/images/user.png',
        firstName: '',
        lastName: '',
        phone: '',
      },
      loading: false,
      filePath: '',
      imgSource: '',
      imageUri: '',
    };
  }

  static navigationOptions = {
    headerTintColor: colors.white,
    title: 'Profile',
    headerStyle: {
      backgroundColor: colors.themeDefault,
      elevation: 0,
    },
    headerLeft: () => <LeftComponentWhite />,

    headerTitleStyle: {
      color: colors.white,
      textAlign: 'center',
    },
    headerTitleAlign: 'center',
  };
  chooseFile = () => {
    var options = {
      title: 'Take Image From',
      customButtons: [{name: 'customOptionKey', title: ''}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        this.setState({
          imgSource: source,
          imageUri: response.path,
        });
      }
    });
  };

  handleSubmit = () => {
    var params = this.state.user;
    if (params['firstName'] == '') {
      Helper.showSnack('Enter your first name.');
      return false;
    }
    if (params['lastName'] == '') {
      Helper.showSnack('Enter your last name.');
      return false;
    }
    if (params['email'] == '') {
      Helper.showSnack('Enter your email address.');
      return false;
    }
    if (params['phone'] == '') {
      Helper.showSnack('Enter your phone number.');
      return false;
    }
    var userParams = {
      firstName: params.firstName,
      lastName: params.lastName,
      phone: params.phone,
      email: params.email,
      userId: params.userId,
      sessionId: params.sessionId,
    };
    userParams['image'] = '';
    console.warn(params);
    if (this.state.imageUri != '') {
      userParams['image'] = 'file://' + this.state.imageUri;
    }
    this.setState({loading: true});
    updateProfile(userParams).then((result) => {
      if (result.status == 200) {
        saveUser('user', result.data.user);
        Helper.updateUserObject(result.data.user);
        this.setState({loading: false});
        Alert.alert(
          'Success',
          result.message,
          [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.navigate(this.state.user.userType == 2 ? 'ManagerSettings': 'DriverSettings');
              },
            },
          ],
          {cancelable: false},
        );
      } else if (result.status == 400) {
        this.setState({loading: false});
        Helper.showSnack(result.message);
      } else {
        this.setState({loading: false});
        Helper.showSnack(result.message);
        this.props.navigation.navigate('Logout');
      }
    });
  };

  async componentDidMount() {
    this.props.navigation.addListener('willFocus', () => {
      this.getUserObject();
    });
    this.getUserObject();
  }

  async getUserObject() {
    var user = await AsyncStorage.getItem('user');
    user = JSON.parse(user);
    this.setState({user: user});
  }

  onChange = (value, field) => {
    var params = this.state.user;
    params[field] = value;
    this.setState({user: params});
  };
  

  render() {
    const {photo} = this.state;

    return (
      <SafeAreaView style={[appStyle.flex1]}>
        <View style={[appStyle.mainContainerSettings]}>
          <View style={style.settingsHeader}>
            <TouchableOpacity
              onPress={this.chooseFile}
              style={{position: 'relative'}}>
              <View style={[]}>
                {/* {this.state.imgSource != '' ? (
                  <Image source={this.state.imgSource} style={image.profile} />
                ) : ( */}
                  <Image
                    style={image.profile}
                    source={{uri: this.state.user.image}}
                  />
                {/* )} */}
              </View>

              {/* <Image
                source={images.plus}
                style={[image.icon, style.addImgIcon]}
              /> */}
            </TouchableOpacity>

            <View style={[appStyle.pl15]}>
              <Text style={[text.h1, text.white, text.medium]}>
                {this.state.user.firstName + ' ' + this.state.user.lastName}
              </Text>
              <Text style={[text.h3, text.white, text.medium]}>
                {this.state.user.phone}
              </Text>
            </View>
          </View>
          <KeyboardAwareScrollView
            contentContainerStyle={[appStyle.scrollContainerCenter]}>
            <View style={[appStyle.wrapper]}>
              <View style={[appStyle.mb10]}>
                <Text style={[text.title]}>Basic Information</Text>
              </View>
              <View style={[input.formInput]}>
                <TextInput
                  style={input.input}
                  placeholder={'First Name'}
                  placeholderTextColor={colors.placeholder}
                  onChangeText={(Text) => this.onChange(Text, 'firstName')}
                  value={this.state.user.firstName}></TextInput>
              </View>
              <View style={[input.formInput]}>
                <TextInput
                  style={input.input}
                  placeholder={'Last Name'}
                  placeholderTextColor={colors.placeholder}
                  onChangeText={(Text) => this.onChange(Text, 'lastName')}
                  value={this.state.user.lastName}></TextInput>
              </View>
              <View style={[input.formInput]}>
                <TextInput
                  style={input.input}
                  placeholder={'Email Address'}
                  keyboardType="numeric"
                  placeholderTextColor={colors.placeholder}
                  onChangeText={(Text) => this.onChange(Text, 'email')}
                  value={this.state.user.email}></TextInput>
              </View>
              <View style={[input.formInput]}>
                <TextInput
                  style={input.input}
                  placeholder={'Phone Number'}
                  keyboardType="numeric"
                  placeholderTextColor={colors.placeholder}
                  onChangeText={(Text) => this.onChange(Text, 'phone')}
                  value={this.state.user.phone}></TextInput>
              </View>
              {/* <Input placeholder={'First Name'} />
              <Input placeholder={'Last Name'} />
              <Input placeholder={'Phone'} /> */}

              <ButtonDefault onPress={() => this.handleSubmit()}>
                Update
              </ButtonDefault>

              <View></View>
            </View>

            <Dialog
              dialogStyle={appStyle.dialogLoader}
              visible={this.state.loading}
              dialogAnimation={new ScaleAnimation()}
              overlayOpacity={0}>
              <DialogContent>
                <View style={appStyle.dialogActivityContainer}>
                  <ActivityIndicator size="large" color={colors.red} />
                </View>
              </DialogContent>
            </Dialog>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

async function saveUser(user, data) {
  try {
    await AsyncStorage.removeItem('user');
    await AsyncStorage.setItem(user, JSON.stringify(data));
  } catch (error) {}
}
