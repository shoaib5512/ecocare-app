import {Alert} from 'react-native';
var firebase = require('firebase');
import Snackbar from 'react-native-snackbar';
import * as Geolib from 'geolib';
import {getSession} from '../network/Session';
export function showAlert(title, message) {
  var result = Alert.alert(
    title,
    message,
    [{text: 'OK', onPress: () => console.log('OK Pressed')}],
    {cancelable: false},
  );
  return result;
}

export function getConversationId(currentUserId, chatUserId) {
  if (currentUserId < chatUserId) {
    return currentUserId + '_' + chatUserId;
  } else {
    return chatUserId + '_' + currentUserId;
  }
}

export function convertUnixIntoTime(unixDate) {
  var date = unixDate;
  var time = new Date(date);

  var hours = time.getHours();
  var minutes = time.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var formattedTime = hours + ':' + minutes + '' + ampm;

  var month = time.getMonth() + 1;
  const formattedDate = month + '/' + time.getDate() + '/' + time.getFullYear();
  return formattedDate + ' ' + formattedTime;
}

export function convertTimeToString(timeInMiliSeconds) {
  var yesterday = timeInMiliSeconds - 1000 * 60 * 60 * 24 * 2; // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
  yesterday = new Date(yesterday);
  return yesterday;
}

//add notification
export function addNotification(params) {
  firebase
    .database()
    .ref('Notification' + '/' + params.userId)
    .push(params)
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.log('error ', error);
    });
}

//=get distance
export function getDistance(num) {
  num = Number(num);
  if (num <= 0) {
    num = num + ' miles';
  } else if (num == 0) {
    num = '0 miles';
  } else {
    // num = num / 1000;
    num = num.toFixed(2) + ' miles';
  }
  return num;
}

//update user
export function updateUserObject(params) {
  firebase
    .database()
    .ref('Users' + '/' + params.userId)
    .set(params)
    .catch((error) => {
      console.log('error ', error);
    });
}

export function getUserObject(userId) {
  firebase
    .database()
    .ref('Users')
    .child(userId)
    .on('value', (user) => {
      return user.val();
    });
}

export function updateUserLocation(params) {
  firebase
    .database()
    .ref('location' + '/' + params.userId)
    .set(params)
    .catch((error) => {
      console.log('error ', error);
    });
}


export async  function updateGeoLocation(lat, long){
  var user = await getSession();
  if (user != null) {
    if (user.userType == 3 && Number(user.isOnline) == 1) {
      var params = {
        userId: user.userId,
        latitude: lat,
        longitude: long,
      };
      firebase
      .database()
      .ref('location' + '/' + params.userId)
      .set(params)
      .catch((error) => {
        console.log('error ', error);
      });
    }
  }

}
// snackbar
export function showSnack(message) {
  Snackbar.show({
    text: message,
    duration: Snackbar.LENGTH_LONG,
  });
}

export function calculateDistance(lat1, lon1, lat2, lon2) {
  // var dis = await calculateDistance(lat1,  lon1,lat2, lon2);
  // return dis;
  Number.prototype.toRad = function () {
    return (this * Math.PI) / 180;
  };
  // console.warn(lat1, lat2, lon1, lon2);
  var lat2 = Number(lat2);
  var lon2 = Number(lon2);
  var lat1 = Number(lat1);
  var lon1 = Number(lon1);

  var R = 6371; // km
  //has a problem with the .toRad() method below.
  var x1 = lat2 - lat1;
  var dLat = x1.toRad();
  var x2 = lon2 - lon1;
  var dLon = x2.toRad();
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1.toRad()) *
      Math.cos(lat2.toRad()) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  // d = parseFloat(d).toFixed(1);
  // if (Number(d) < 1) {
  //   var d = d * 1000;
  //   d = d + 'm',
  // } else {
  //   d = d + 'km';
  // }

  return Number(d);
}

export function getGeoDistance(lat1, lon1, lat2, lon2) {
  var lat1 = Number(lat1);
  var lon1 = Number(lon1);
  var lat2 = Number(lat2);
  var lon2 = Number(lon2);

  var d = Geolib.getPreciseDistance(
    {latitude: lat1, longitude: lon1},
    {latitude: lat2, longitude: lon2},
  );
  return d / 1000;
}
