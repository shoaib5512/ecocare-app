import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    View,
    Image,
    TextInput,
    ImageBackground
} from 'react-native';

import { colors, screenHeight, screenWidth, images,styleSet,hitArea } from '../config/Constant';

import appStyle from '../assets/styles/appStyle';
import style from '../assets/styles/style';
import image from '../assets/styles/image';
import text from '../assets/styles/text';
import button from '../assets/styles/button';
import input from '../assets/styles/input';
// var FloatingLabel = require('react-native-floating-labels');




const ButtonDefault = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            style={[button.buttonTheme, { maxWidth: props.maxWidth ,display: props.display}]}
        >
            <Text style={[text.h2, text.letterSpace, text.regular, { color: colors.white }]}>
                {props.children}
            </Text>
        </TouchableOpacity>
    )
};

export { ButtonDefault };

const ButtonDefaultSlim = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            style={[button.buttonTheme, { paddingVertical: 5,maxWidth: props.maxWidth ,display: props.display}]}
        >
            <Text style={[text.h2, text.letterSpace, text.regular, { color: colors.white }]}>
                {props.children}
            </Text>
        </TouchableOpacity>
    )
};

export { ButtonDefaultSlim };

const ButtonDefaultwhite = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            style={[button.buttonTheme, { maxWidth: props.maxWidth,backgroundColor:colors.white }]}
        >
            <Text style={[text.h2, text.letterSpace, text.robotoMedium, { color: colors.themeDefault }]}>
                {props.children}
            </Text>
        </TouchableOpacity>
    )
};

export { ButtonDefaultwhite };


const ButtonOutline = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            style={[button.buttonThemeOutline, { maxWidth: props.maxWidth }]}
        >
            <Text style={[text.text15, text.letterSpace,text.center, { color: colors.themeSecondary }]}>
                {props.children}
            </Text>
        </TouchableOpacity>
    )
};

export { ButtonOutline };

const ButtonText = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            style={{ maxWidth: props.maxWidth,paddingVertical:5}}
        >
            <Text style={[text.h3, { color: props.color }]}>
                {props.children}
            </Text>
        </TouchableOpacity>
    )
};

export { ButtonText };


const Input = (props) => {
    return (
        <View style={[input.formInput]}>
        <TextInput 
            style={input.input}
            placeholder={props.placeholder}
            placeholderTextColor={colors.placeholder}
            secureTextEntry={props.secure}
            value={props.value}
            keyboardType={props.keyboard}
        >
        </TextInput>
        </View>
    )
};

export { Input };




const ActionSheetButton = (props) => {
    return (
        <TouchableOpacity 
        hitSlop={hitArea.hitArea}
        onPress={props.onPress}
        style={{width:50,height:3,
          backgroundColor:colors.themeSecondary,
          borderRadius: styleSet.bRadiusTheme,
          alignSelf:'center',marginTop:10,shadowColor: 'rgba(0,0,0,0.16)',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.16,
          shadowRadius: 3,
          
          elevation: 1,}}/>
    )
};

export { ActionSheetButton };
