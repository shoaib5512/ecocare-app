import React, {Component} from 'react';
import {withNavigation} from 'react-navigation';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {DrawerActions} from 'react-navigation-drawer';
import {Image} from 'react-native';
class HamburgerIcon extends Component {
    toggleDrawer = () => {
        //Props to open/close the drawer
        this.props.navigationProps.toggleDrawer();
      };
  render() {
    return (
      <TouchableOpacity
        style={{
          width: 40,
          height: 40,
          marginLeft: 5,
          resizeMode:'contain',
          justifyContent:'center'
        }}
        onPress={()=>this.props.navigation.dispatch(DrawerActions.toggleDrawer())}>
         <Image style={{width: 27,
          height: 27,
          resizeMode:'contain'}} source={require('../../assets/images/iconHam.png')}
         />
      </TouchableOpacity>
    );
  }
}
export default withNavigation(HamburgerIcon);