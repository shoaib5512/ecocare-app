import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Image, Text, View } from 'react-native';
import { colors, screenHeight, screenWidth, images, styleSet } from '../../config/Constant';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import style from '../../assets/styles/style';
import { createAppContainer } from 'react-navigation';
import { NavigationContainer } from '@react-navigation/native';


const RouteButton = (props) => {
  return (
    <View style={{ flexWrap: 'wrap' }}>
      <TouchableOpacity
        onPress={props.onPress}
        style={{
          backgroundColor: '#fff',
          padding: 10,
          borderRadius: 10,
          shadowColor: 'rgba(0,0,0,0.16)',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.16,
          shadowRadius: 3.0,

          elevation: 6,
          marginRight:15
        }}
        hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
      >
        <Image style={{
          width: 20, height: 20, resizeMode: 'contain'
        }} source={images.locationFrom} />
      </TouchableOpacity>
    </View>

  )
};

export { RouteButton };




