import React, { Component, props } from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Image, Text, View } from 'react-native';
import { colors, screenHeight, screenWidth, images,styleSet } from '../../config/Constant';

import appStyle from '../../assets/styles/appStyle';
import image from '../../assets/styles/image';
import text from '../../assets/styles/text';
import input from '../../assets/styles/input';
import button from '../../assets/styles/button';

class LeftComponentWhite extends Component {
  render(props) {
    return (
      <View>
        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} 
          onPress={() => this.props.navigation.goBack()}>
          <Image style={[styleSet.backArrowStyle,{tintColor: '#fff'}]} source={images.back} />
        </TouchableOpacity>
      </View>
    );
  }
}








export default withNavigation(LeftComponentWhite);


