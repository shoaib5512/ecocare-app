import { Dimensions } from 'react-native';
import React, { Component } from 'react';
export const url = {
  //base: 'https://test-rexsolution.com/example/api/',
  //signup: 'UserService/signup',
  //login: 'UserService/login',
};

//Size

export const screenHeight = {
  height20: Math.round((20 / 100) * Dimensions.get('window').height),
  height30: Math.round((40 / 100) * Dimensions.get('window').height),
  height35: Math.round((35 / 100) * Dimensions.get('window').height),
  height40: Math.round((40 / 100) * Dimensions.get('window').height),
  height50: Math.round((50 / 100) * Dimensions.get('window').height),
  height60: Math.round((60 / 100) * Dimensions.get('window').height),
  height70: Math.round((70 / 100) * Dimensions.get('window').height),
  height100: Math.round(Dimensions.get('window').height),
};

export const screenWidth = {
  width100: Math.round(Dimensions.get('window').width),
  width50: Math.round((50 / 100) * Dimensions.get('window').width),
  width45: Math.round((45 / 100) * Dimensions.get('window').width),
  width40: Math.round((40 / 100) * Dimensions.get('window').width),
  width35: Math.round((35 / 100) * Dimensions.get('window').width),
  width30: Math.round((30 / 100) * Dimensions.get('window').width),
  width15: Math.round((15 / 100) * Dimensions.get('window').width),

  width25: Math.round((25 / 100) * Dimensions.get('window').width),
  width20: Math.round((20 / 100) * Dimensions.get('window').width),
  width65: Math.round((65 / 100) * Dimensions.get('window').width),
  width70: Math.round((70 / 100) * Dimensions.get('window').width),
  width80: Math.round((80 / 100) * Dimensions.get('window').width),
  width90: Math.round((90 / 100) * Dimensions.get('window').width),
  width18: Math.round((18 / 100) * Dimensions.get('window').width),

};

//colors
export const colors = {
  themeDefault: '#D91E3E',
  themeSecondary: '#707070',
  placeholder : '#C6C8CB',
  black: '#231F20',
  white: '#fff',
  pureWhite: '#F9F9F9',
  grey: '#B1B3B7',
  skyBlue: '#4282E2',
  shadowColor: '#F9F9F9',
  //
  greyDark: '#919191',
  green: '#7BB858',
  regularGray: '#707070',
  lightGray: '#E5E5E5',
  red: '#C91E1E',

  lightWhite: 'rgba(255,255,255,1)',
  lightBrown: '#ebe9e8',


};

//Images
export const images = {
  //splash

  //app Images
  logo: require('../assets/images/logo.png'),
  logoDark: require('../assets/images/logoDark.jpg'),
  location: require('../assets/images/Icons/location.png'),
  locationTo: require('../assets/images/Icons/location-to.png'),
  locationFrom: require('../assets/images/Icons/location-from.png'),
  marker: require('../assets/images/Icons/marker.png'),
  end: require('../assets/images/Icons/end.png'),
  phone: require('../assets/images/phone.png'),
  start: require('../assets/images/Icons/start.png'),
  locationLine: require('../assets/images/Icons/location-line.png'),

  route: require('../assets/images/Icons/route.png'),
  complete: require('../assets/images/Icons/complete.png'),
  current: require('../assets/images/Icons/current.png'),
  upcoming: require('../assets/images/Icons/upcoming.png'),


  routePointEmpty: require('../assets/images/Icons/routePointEmpty.png'),
  routePointFilled: require('../assets/images/Icons/routePointFilled.png'),
  lineRouteHorizontal: require('../assets/images/Icons/lineRouteHorizontal.png'),





 
  
  tab1: require('../assets/images/Icons/tab1.png'),
  tab2: require('../assets/images/Icons/tab2.png'),
  tabHome: require('../assets/images/Icons/tabHome.png'),

  
  arrow: require('../assets/images/Icons/darkrightArrow.png'),
  user: require('../assets/images/Icons/profile-icon.png'),
  bell: require('../assets/images/Icons/bell-icon.png'),
  history: require('../assets/images/Icons/history-icon.png'),
  logout: require('../assets/images/Icons/logout.png'),
  plus: require('../assets/images/Icons/plus.png'),
  back: require('../assets/images/Icons/backArrow.png'),


  // others

  dummyUser1: require('../assets/images/other/user-img.jpg'),




};

export const navThemeConstants = {
  light: {
    backgroundColor: '#fff',
    fontColor: '#000',
    headerStyleColor: '#E8E8E8',
    iconBackground: '#F4F4F4',
  },
  dark: {
    backgroundColor: '#000',
    fontColor: '#fff',
    headerStyleColor: 'E8E8E8',
    iconBackground: '#e6e6f2',
  },
};

export const fontSet = {
  xxlarge: 40,
  xlarge: 30,
  large: 20,
  middle: 18,
  normal: 14,
  small: 12,
  xsmall: 11,
};

export const sizeSet = {
  tabIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
};

export const hitArea = {
  hitArea: { top: 20, bottom: 20, left: 50, right: 50 },
};

export const layout = {
  screenSpace: 20,
  screenSpaceBottom : 40
};

export const styleSet = {
  searchBar: {
    container: {
      marginLeft: Platform.OS === 'ios' ? 30 : 0,
      backgroundColor: 'transparent',
      borderBottomColor: 'transparent',
      borderTopColor: 'transparent',
      flex: 1,
    },
    input: {
      backgroundColor: colors.inputBgColor,
      borderRadius: 10,
      color: 'black',
    },
  },
  rightNavButton: {
    marginRight: 10,
  },
  backArrowStyle: {
    resizeMode: 'contain',
    // tintColor: '#eb5a6d',
    width: 25,
    height: 22,
    // marginTop: Platform.OS === 'ios' ? 50 : 20,
    marginLeft: 15,
    alignSelf: 'center',
  },
  bellButtonStyle: {
    resizeMode: 'contain',
    // tintColor: '#eb5a6d',
    width: 20,
    height: 20,
    // marginTop: Platform.OS === 'ios' ? 50 : 20,
    marginRight: 15,
    alignSelf: 'center',
  },
  bRadiusTheme: 8,
  shadow: {
    shadowColor: 'rgba(0,0,0,0.16)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.16,
    shadowRadius: 3.0,

    elevation: 6,
  },
};
