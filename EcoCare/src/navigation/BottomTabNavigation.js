import React from 'react';
import { Button, Image, Text, View, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Settings from '../views/driver/Settings';
import ProfileSettings from '../views/driver/ProfileSettings';
import History from '../views/driver/History';


import TaskScreen from '../views/driver/Task';
import Route from '../views/driver/Route';
import RouteMap from '../views/driver/RouteMap';
import Scanner from '../views/admin/TaskScanner';
import ScannerResult from '../views/admin/QRDetail';





import { createStackNavigator } from 'react-navigation-stack';

import {
  colors,
  screenHeight,
  screenWidth,
  images,
  sizeSet,
} from '../config/Constant';








// add new screen to this stack here
const TaskStack = createStackNavigator({
  Task: TaskScreen,
  DriverTaskRoutes: Route,
  DriverRouteMap:RouteMap,
  DriverScannner:Scanner,
  DriverScannnerResult:ScannerResult,

  
});
TaskStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;

  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

// add new screen to this stack here
const SettingsStack = createStackNavigator({
  DriverSettings: Settings,
  DriverProfileSettings: ProfileSettings,
  History: History
  
});


SettingsStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;

  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};








const TabNavigator = createMaterialBottomTabNavigator(
  {
    DriverTask: {
      screen: TaskStack,
      navigationOptions: {
        tabBarLabel: () => null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={[sizeSet.tabIcon, { tintColor: tintColor }]}
            source={images.tab1}
          />
        ),

      },
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: {
        tabBarLabel: () => null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={[sizeSet.tabIcon, { tintColor: tintColor }]}
            source={images.tab2}
          />
        ),

      },
    },
  },
  {
    initialRouteName: 'DriverTask',
    activeColor: colors.themeDefault,
    activeBackgroundColor: 'red',
    inactiveBackgroundColor: 'blue',
    inactiveColor: colors.greyDark,
    barStyle: {
      backgroundColor: colors.pureWhite
     
    },

  },
);

export default createAppContainer(TabNavigator);
