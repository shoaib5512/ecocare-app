import React from 'react';
import { Button, Image, Text, View, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Settings from '../views/driver/Settings';
import ProfileSettings from '../views/driver/ProfileSettings';



import Task from '../views/manager/Task';
import Drivers from '../views/manager/Drivers';
import DriverTask from '../views/driver/Task';
import Route from '../views/driver/Route';
import Home from '../views/manager/Home';

import RouteMap from '../views/driver/RouteMap';
import Scanner from '../views/admin/TaskScanner';
import ScannerResult from '../views/admin/QRDetail';

import { createStackNavigator } from 'react-navigation-stack';

import {
  colors,
  screenHeight,
  screenWidth,
  images,
  sizeSet,
} from '../config/Constant';




// add new screen to this stack here
const HomeStack = createStackNavigator({
  Home: Home,
  ManagerTaskRoutes : Route,
  ManagerRouteMap:RouteMap
});

HomeStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;

  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


// add new screen to this stack here
const TaskStack = createStackNavigator({
  Drivers : Drivers,
  DriverTask : DriverTask,
  DriverTaskRoutes : Route,
  DriverRouteMap:RouteMap,
  ManagerScannner:Scanner,
  ManagerScannnerResult:ScannerResult,
});


// add new screen to this stack here
const SettingsStack = createStackNavigator({
  ManagerSettings: Settings,
  ManagerProfileSettings: ProfileSettings,
  
});

SettingsStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;

  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


const ManagerTabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: () => null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={[sizeSet.tabIcon, { tintColor: tintColor }]}
            source={images.tabHome}
          />
        ),
      },
    },
    Task: {
      screen: TaskStack,
      navigationOptions: {
        tabBarLabel: () => null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={[sizeSet.tabIcon, { tintColor: tintColor }]}
            source={images.tab1}
          />
        ),

      },
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: {
        tabBarLabel: () => null,
        tabBarIcon: ({ tintColor }) => (
          <Image
            style={[sizeSet.tabIcon, { tintColor: tintColor }]}
            source={images.tab2}
          />
        ),

      },
    },
  },
  {
    initialRouteName: 'Home',
    activeColor: colors.themeDefault,
    activeBackgroundColor: 'red',
    inactiveBackgroundColor: 'blue',
    inactiveColor: colors.greyDark,
    barStyle: {
      backgroundColor: colors.pureWhite
     
    },

  },
);

export default createAppContainer(ManagerTabNavigator);
