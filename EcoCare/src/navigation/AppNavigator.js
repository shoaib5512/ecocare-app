import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import DriverTabNavigator from '../navigation/BottomTabNavigation';
import ManagerTabNavigator from '../navigation/BottomTabNavigationManager';

import Splash from '../views/registration/Splash';
import Login from '../views/registration/Login';
import ForgotPassword from '../views/registration/ForgotPassword';
import ForgotOtp from '../views/registration/ForgotOtp';
import LogoutScreen from '../views/registration/Logout';
import LocationPermission from '../views/registration/LocationPermission';
//admin
import Admin from '../views/admin/Admin';
import TaskScanner from '../views/admin/TaskScanner';
import QRDetail from  '../views/admin/QRDetail';
const DisclosureStack = createStackNavigator(
  {
    LocationPermission: LocationPermission,
  },

  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);
const SplashStack = createStackNavigator(
  {
    Splash: Splash,
  },

  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

const AuthStack = createStackNavigator(
  {
    Login: Login,
    ForgotPassword: ForgotPassword,
    ForgotOtp: ForgotOtp,
    LocationPermission: LocationPermission,
  },

  // {
  //   headerMode: 'none',
  //   navigationOptions :{
  //     headerVisible: false,
  //   }
  // }
);

const DriverNavigationStack = createStackNavigator(
  {
    screen: DriverTabNavigator,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

const ManagerNavigationStack = createStackNavigator(
  {
    screen: ManagerTabNavigator,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

const AdminNavigationStack = createStackNavigator(
  {
    Admin: Admin,
    TaskScanner: TaskScanner,
    QRDetail: QRDetail,
  },
  // {
  //   headerMode: 'none',
  //   navigationOptions: {
  //     headerVisible: false,
  //   },
  // },
);
const LogoutStack = createStackNavigator(
  {
    Logout: LogoutScreen,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

export default createAppContainer(
  createSwitchNavigator(
    {
      Disclosure: DisclosureStack,
      Splash: SplashStack,
      Auth: AuthStack,
      DriverNavigation: DriverNavigationStack,
      ManagerNavigation: ManagerNavigationStack,
      AdminNavigationStack: AdminNavigationStack,
      Logout: LogoutStack,
    },

    {
      initialRouteName: 'Disclosure',
    },
  ),
);
